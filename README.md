# Atlassian Cloud for Gmail

## Useful links

* [Add-on in G Suite Marketplace](https://gsuite.google.com/marketplace/app/atlassian_cloud/689852235462)

## Pre-requisites

- Make sure you have the latest Node.js (we're currently running on Node **10.x**)
- Install [clasp](https://www.npmjs.com/package/@google/clasp) globally: `npm i -g @google/clasp`
- Enable **Google Apps Script** for your Google Drive Account:

    - Go to [Drive](https://drive.google.com)
    - Click **New** > **More** > **Connect more apps**
    - Search for `Google Apps Script` and click **Connect**
    - Turn on Google Apps Script API in your [user settings](https://script.google.com/home/usersettings)

- Authorize **Clasp** to access your Google Account: `clasp login`

## Creating and deploying your test app

- Create a new Google Apps Script project and link it to your local repo:
    - Install your local dependencies by running `npm i` in project's root directory
    - Run `clasp create "Atlassian Cloud for Gmail (your_name)"`
    - All your Apps Script projects live at [script.google.com](https://script.google.com/)
    - You can open current project in Google Apps Script UI by running `clasp open` in project dir

- Configure your Bitbucket OAuth consumer

    - Go to your [Bitbucket OAuth settings](https://bitbucket.org/account/admin/api)
    - Under **OAuth Consumers** click **Add consumer**
    - Enter the following:

        - **Name:** `Atlassian Cloud for Gmail (your_name)`
        - **Description:** `Atlassian Cloud for Gmail (your_name)`
        - **Callback URL:** `https://script.google.com/macros/d/your_script_id/usercallback` (replacing `your_script_id` with the value you can find in `.clasp.json`)
        - **Uncheck** `This is a private consumer` (*Important!*)

    - Grant the following permissions:

        - `account:write`
        - `team membership:read`
        - `projects:read`
        - `repositories:write`
        - `pull requests:write`
        - `issues:write`
        - `wikis:read and write`
        - `snippets:write`
        - `pipelines:write`

    - Click **Save**
    - Find your newly created consumer under **OAuth Consumers** and expand it
    - Copy the **Key** and **Secret**
    - Open your Google Apps Script project (`clasp open`)
    - Click **File** > **Project properties** > **Script properties**
    - Click **Add row**
    - Enter the property name `bitbucketCredentials`
    - Enter the value `{"clientId": "bitbucket_oauth_key", "clientSecret": "bitbucket_oauth_secret" }` (substituting `bitbucket_oauth_key` and `bitbucket_oauth_secret` with the values above)
    - Click **Save**

- Configure your Atlassian app and OAuth consumer

    - Go to https://developer.atlassian.com/apps/create
    - Enter an app name: `Atlassian Cloud for Gmail (your_name)` and click **Create**
    - Enable the **Jira API**
    - Add the following API scopes:

        - `Access your personal account details`
        - `View all issues and projects that you can see in Jira`
        - `Create issues and progress on them`

    - Click **Inbound authorization**
    - Turn on the toggle switch
    - Enter the **Callback URL**: `https://script.google.com/macros/d/your_script_id/usercallback` (replacing `your_script_id` with the value you can find in `.clasp.json`)
    - Click **Save**
    - Return to **Enabled APIs**
    - Copy the **Client Id** and **Client Secret**
    - Open your Google Apps Script project (`clasp open`)
    - Click **File** > **Project properties** > **Script properties**
    - Click **Add row**
    - Enter the property name `atlassianCredentials`
    - Enter the value `{"clientId": "app_client_id", "clientSecret": "app_client_secret" }` (substituting `app_client_id` and `app_client_secret` with the values above)
    - Click **Save**

- Configure Google Analytics (not required for local development)

    - Sign Up to [Google Analytics](https://www.google.com/analytics)
    - Choose Mobile App
    - Copy **Tracking Id**
    - Open your Google Apps Script project (`clasp open`)
    - Click **File** > **Project properties** > **Script properties**
    - Click **Add row**
    - Enter the property name `googleAnalytics`
    - Enter the value `{"trackingId": "your_tracking_id"}`

- Push your local code to Google Apps Script project

    - Run `npm run build` and `clasp push` from the root of the project
    - Browse back to your Google Apps Script project in the UI. Press the refresh button in your browser (don't press ⌘-R, Google have overloaded it in their editor 🤦‍). There should be code now. 🎉

- Install the **Atlassian Cloud for Gmail** app in Gmail

    - Run `clasp deployments` and copy the `Deployment ID` of @HEAD (e.g. `AKfycbz2Iv3-uCdmatxfopJOMbyqWAmpD21bdJD2B_aVBLGO`)
    - Go to [https://mail.google.com/mail/#settings/addons](https://mail.google.com/mail/#settings/addons)
    - Make sure you're signed in to the same account as you created the addon under (you must have read access to the add-on to be able to install it)
    - Enter the `Deployment ID` you copied into the field next to **Developer add-ons**
    - Click **Install**
    - Confirm that you trust yourself 😛 and then click **Install**
    - Browse to an email with a link to a Bitbucket pull request, issue, or pipeline result
    - Wait for the Atlassian logo to appear in the right hand bar, then click on it
    - Click **Authorize** then **Grant access** in the pop-up that appears

🍾 Congratulations, the add-on should now be installed and working!

## Development

Run `npm start` this will watch for the changes and push your local code to GAS automatically.

## Deployment Configuration

- Configure $REFRESH_TOKEN, $STAGING_SCRIPT_ID, $PROD_SCRIPT_ID environment variables for Pipelines.

    - You can find REFRESH_TOKEN in your `~/.clasprc.json` after successfully completed `clasp login`
    - You can find SCRIPT_ID for particular project by opening `File` -> `Project properties` -> `Script ID` in GAS Editor
