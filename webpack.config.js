const webpack = require('webpack')
const path = require('path')
const fs = require('fs')
const shell = require('shelljs')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const manifest = require('./appsscript')

/*
 * Executes command in shell
 */
function RunCommandPlugin (command) {
  this.apply = (compiler) => {
    compiler.hooks.afterEmit.tap('Run Command', (compilation) => {
      if (!compilation.errors.length) {
        setImmediate(() => shell.exec(command))
      }
    })
  }
}

/**
 * Writes JSON content to file
 */
function WriteJsonPlugin (options) {
  this.apply = (compiler) => {
    compiler.hooks.afterEmit.tap('Create File', () => {
      fs.writeFileSync(options.path, JSON.stringify(options.content, null, 2))
    })
  }
}

module.exports = ({ claspPush } = {}) => {
  const plugins = [
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/), // ignore moment-locales to reduce bundle size
    new CopyWebpackPlugin(['static/_.js']),
    new WriteJsonPlugin({
      path: `${__dirname}/appsscript.json`,
      content: manifest,
    }),
  ]

  if (claspPush) {
    plugins.push(new RunCommandPlugin('node_modules/.bin/clasp push'))
  }

  return {
    entry: './src/index.js',
    output: {
      path: path.join(__dirname, 'build'),
      libraryTarget: 'this',
      filename: 'bundle.js',
    },
    mode: 'development',
    devtool: 'hidden-source-map',
    module: {
      rules: [
        {
          test: /\.js$/,
          include: path.resolve(__dirname, 'src'),
          use: [
            'babel-loader?cacheDirectory=true',
            'eslint-loader?cache=true',
          ],
        },
      ],
    },
    plugins,
  }
}
