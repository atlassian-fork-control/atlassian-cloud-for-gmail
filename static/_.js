// This file is used to prevent Google Script UI from opening and indexing bundle.js
// as it stucks on this process and your browser could just crash 🤦‍
// So by default this file will be opened first in Google Script Editor.
