import { getStatusText } from 'http-status-codes'

export default class HttpClient {
  constructor () {
    this._requests = []
  }

  setParallel (parallel) {
    this._parallelMode = parallel
  }

  isParallel () {
    return this._parallelMode
  }

  enqueueRequest (url, { method = 'GET', body, headers = {} } = {}) {
    if (headers['Content-Type'] === undefined) {
      headers['Content-Type'] = 'application/json'
    }

    if (body && headers['Content-Type'] === 'application/json') {
      body = JSON.stringify(body)
    }

    this._requests.push({
      url,
      method,
      headers,
      payload: body,
      muteHttpExceptions: true,
    })
  }

  execute () {
    const responses = UrlFetchApp.fetchAll(this._requests)

    const sentRequests = this._requests.slice()
    this._requests = [] // flush enqueued requests

    const results = responses.map((response, index) => {
      const rawBody = response.getContentText()
      const headers = response.getHeaders()
      const statusCode = response.getResponseCode()

      const isJSON = (headers['Content-Type'] || '').includes('application/json')
      const body = (isJSON && rawBody) ? JSON.parse(rawBody) : rawBody

      if (statusCode < 400) {
        return body
      }

      throw Object.assign(
        new Error(getStatusText(statusCode)),
        {
          request: sentRequests[index],
          response: { statusCode, headers, body },
        }
      )
    })

    if (this.isParallel()) {
      this.setParallel(false)
      return results
    }

    return results[0]
  }
}
