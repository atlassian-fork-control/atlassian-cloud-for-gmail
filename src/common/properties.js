class CacheablePropertiesService {
  constructor (properties, cache) {
    this.properties = properties
    this.cache = cache
  }

  get (key) {
    const parse = (value) => {
      try {
        return JSON.parse(value)
      } catch (error) {
        return value // return plain string
      }
    }

    const cachedValue = this.cache.get(key)
    if (cachedValue) {
      return parse(cachedValue)
    }

    const value = this.properties.getProperty(key)
    if (value) {
      this.cache.put(key, value)
      return parse(value)
    }
  }

  set (key, value) {
    const json = JSON.stringify(value)
    this.cache.put(key, json)
    this.properties.setProperty(key, json)
  }

  remove (key) {
    this.cache.remove(key)
    this.properties.deleteProperty(key)
  }
}

export const userProperties = new CacheablePropertiesService(
  PropertiesService.getUserProperties(),
  CacheService.getUserCache()
)

export const scriptProperties = new CacheablePropertiesService(
  PropertiesService.getScriptProperties(),
  CacheService.getScriptCache()
)
