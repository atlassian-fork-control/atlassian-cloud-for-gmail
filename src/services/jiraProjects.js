import moment from 'moment'
import urlParse from 'url-parse'
import Jira from '../common/net/Jira'
import { userProperties } from '../common/properties'
import { copyWithoutSecrets } from '../common/utils'
import AddonError from '../common/AddonError'

export default {
  sync () {
    try {
      const jiraStore = this._getStore()

      if (Number(jiraStore.syncedAt) > moment().subtract(1, 'day').valueOf()) {
        // sync once a day
        return
      }

      const jira = new Jira()
      const accessibleResources = jira.getAccessibleResources()

      jira.parallel()
      accessibleResources.forEach(resource => jira.getServerInfo({ cloudId: resource.id }))
      const serversInfo = jira.fetch()

      const sites = accessibleResources.map((resource, index) => {
        const projectsKeys = new Jira(null, resource.id).getProjects().map(p => p.key)
        const instanceHost = urlParse(serversInfo[index].baseUrl).host
        return { projectsKeys, instanceHost }
      })

      this._saveStore({
        syncedAt: Date.now(),
        sites,
      })
    } catch (error) {
      if (error instanceof AddonError) {
        // we don't care about Addon errors here
        // as they are expected errors
        return
      }

      console.error({
        ...copyWithoutSecrets(error),
        message: `Error during projects sync: ${error.message}`,
      })
    }
  },

  findInstancesByProject (projectKey) {
    const { sites } = this._getStore()

    if (!sites) return []

    return sites
      .filter(site => site.projectsKeys.includes(projectKey))
      .map(site => site.instanceHost)
  },

  /**
   * Google Apps script has a quota on property size - 9kb
   * So we split stored json string into chunks and store
   * every chunk as a separate record to not exceed quota in cases
   * user has a lot of Jira sites authorized or a lot of projects in those sites
   */
  _saveStore (data) {
    const jsonString = JSON.stringify(data)

    // Apps Script runs on Java where 1 char == 2 bytes
    // so split string into chunks with 8 kb size each
    const chunks = jsonString.match(/.{1,4096}/g) // 4096 chars == 8192 bytes

    const chunksReferences = chunks.map((_, i) => `jiraStoreChunk-${i}`)
    chunksReferences.forEach((ref, i) => userProperties.set(ref, chunks[i]))

    userProperties.set('jiraStoreMeta', { chunksReferences })
  },

  _getStore () {
    const jiraStoreMeta = userProperties.get('jiraStoreMeta')

    if (!jiraStoreMeta) {
      return {}
    }

    // merge chunks into complete string
    const jsonString = jiraStoreMeta.chunksReferences
      .map(ref => userProperties.get(ref))
      .join('')

    try {
      return JSON.parse(jsonString)
    } catch (error) {
      return {}
    }
  },
}
