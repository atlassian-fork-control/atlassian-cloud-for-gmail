import View from '../../View'
import Icons from '../../Icons'
import { createActionButtonWidget } from '../../utils'

export default class extends View {
  get header () {
    return CardService.newCardHeader()
      .setTitle('Thank you for the feedback')
      .setImageUrl(Icons.success)
  }

  get sections () {
    const paragraph = CardService.newTextParagraph()
      .setText('Your feedback has been submitted.')

    const action = createActionButtonWidget('Done', 'addon.NavigateBack')

    return CardService.newCardSection()
      .addWidget(paragraph)
      .addWidget(action)
  }
}
