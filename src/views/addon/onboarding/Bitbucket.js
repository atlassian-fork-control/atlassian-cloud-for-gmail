import View from '../../View'
import { ProductIcons, buildImageUrl } from '../../Icons'
import { createKeyValueWidget, createAction } from '../../utils'

export default class extends View {
  get header () {
    return CardService.newCardHeader()
      .setTitle('Connect Bitbucket Cloud')
      .setSubtitle('Step 2 of 2')
      .setImageUrl(ProductIcons.bitbucket)
  }

  get sections () {
    const image = CardService.newImage()
      .setImageUrl(buildImageUrl('onboarding-bitbucket'))

    const paragraph = CardService.newTextParagraph()
      .setText('View, merge, and comment on a pull request, or re-run a failed build, all without leaving your inbox.')

    const authButton = CardService.newTextButton()
      .setText('Connect Bitbucket Cloud')
      .setAuthorizationAction(CardService.newAuthorizationAction()
        .setAuthorizationUrl(this.props.authUrl))

    const rejectionWidget = createKeyValueWidget({
      content: `No thanks, I don't use Bitbucket Cloud`,
      action: createAction('addon.Onboarding', { onboardingState: 'done' }),
    })

    return CardService.newCardSection()
      .addWidget(image)
      .addWidget(paragraph)
      .addWidget(authButton)
      .addWidget(rejectionWidget)
  }
}
