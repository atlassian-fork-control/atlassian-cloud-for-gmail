import View from '../View'
import { ProductIcons, buildImageUrl } from '../Icons'
import { createLinkButtonWidget } from '../utils'

export default class extends View {
  get header () {
    return CardService.newCardHeader()
      .setImageUrl(ProductIcons.atlassian)
      .setTitle(' ') // yes, it should be empty
  }

  get sections () {
    const image = CardService.newImage()
      .setImageUrl(buildImageUrl('onboarding-done'))

    const paragraph0 = CardService.newTextParagraph()
      .setText('Open this add-on the next time you get an email with a Jira Cloud or Bitbucket Cloud link.')
    const paragraph1 = CardService.newTextParagraph()
      .setText('<b>Note:</b> Jira Cloud links typically start with <u>https://mycompany.atlassian.net</u>, Bitbucket Cloud links start with <u>https://bitbucket.org</u>.')
    const paragraph2 = CardService.newTextParagraph()
      .setText('Atlassian Cloud for Gmail does not currently support self-hosted Jira or Bitbucket sites.')

    return CardService.newCardSection()
      .addWidget(image)
      .addWidget(paragraph0)
      .addWidget(paragraph1)
      .addWidget(paragraph2)
      .addWidget(createLinkButtonWidget('Help', 'https://confluence.atlassian.com/cloud/atlassian-cloud-for-gmail-953138538.html'))
  }
}
