import { truncate } from 'lodash'
import View from '../View'
import {
  capitalizeString,
  createAction,
  createKeyValueWidget,
  formatDateTime,
  colorString,
  formatJiraIconUrl
} from '../utils'
import Icons, { BitbucketEntityIcons } from '../Icons'

import Colors, { pickStatusColor } from '../Colors'

export default class extends View {
  get header () {
    return CardService.newCardHeader()
      .setImageUrl(Icons.link)
      .setTitle('Links in this email')
  }

  get sections () {
    const { previews, forbiddenEntities } = this.props

    const previewSections = previews.map(p => this._getPreviewSection(p))
    const jiraForbiddenEntities = forbiddenEntities.filter(e => e.product === 'jira')
    const bitbucketForbiddenEntity = forbiddenEntities.find(e => e.product === 'bitbucket')

    const jiraAuthPromptSections = jiraForbiddenEntities.map(e =>
      this._getJiraAuthorizationPromptSection(e))
    const bitbucketAuthPromptSection = bitbucketForbiddenEntity &&
      this._getBitbucketAuthorizationPromptSection(bitbucketForbiddenEntity)

    return [
      ...previewSections,
      ...jiraAuthPromptSections,
      bitbucketAuthPromptSection,
    ]
  }

  _getPreviewSection (preview) {
    const {
      entity,
      source,
      title,
      key,
      status,
      updatedAt,
      statusCategory,
      iconUrl,
    } = preview

    const action = createAction(`${entity.product}.${entity.name}.Show`, entity.args)
    const statusString = colorString(capitalizeString(status), pickStatusColor(
      statusCategory || status,
      entity.product,
      entity.name,
    ))

    const icon = entity.product === 'jira'
      ? formatJiraIconUrl(iconUrl, entity.args.instanceHost)
      : BitbucketEntityIcons[entity.name]

    return CardService.newCardSection()
      .addWidget(createKeyValueWidget({
        action,
        content: colorString(
          `${capitalizeString(entity.product)} / ${source}`,
          Colors.default
        ),
      }))
      .addWidget(createKeyValueWidget({
        action,
        icon,
        content: [
          `${key} • ${statusString}`,
          truncate(title, { length: 60 }),
        ].join('<br>'),
        bottomLabel: `Last updated ${formatDateTime(updatedAt)}`,
        multiline: true,
      }))
  }

  _getJiraAuthorizationPromptSection (jiraForbiddenEntity) {
    const { args: { instanceHost, issueKey } } = jiraForbiddenEntity
    const authButton = CardService.newTextButton()
      .setText('Connect Jira site')
      .setAuthorizationAction(CardService.newAuthorizationAction()
        .setAuthorizationUrl(this.props.jiraAuthUrl))

    return CardService.newCardSection()
      .addWidget(createKeyValueWidget({
        content: 'Jira',
        icon: Icons.lock,
      }))
      .addWidget(createKeyValueWidget({
        label: instanceHost,
        content: [
          'Connect to Jira site for issue details',
          colorString(issueKey, Colors.default),
        ].join('<br>'),
        multiline: true,
      }))
      .addWidget(authButton)
  }

  _getBitbucketAuthorizationPromptSection (bitbucketForbiddenEntity) {
    const { name, args: { owner, repo, id } } = bitbucketForbiddenEntity
    const key = {
      issue: `Issue #${id}`,
      pullRequest: `Pull Request #${id}`,
      pipeline: `Pipeline`, // we don't have user friendly ID for pipeline in url
    }[name]

    const authButton = CardService.newTextButton()
      .setText('Connect Bitbucket')
      .setAuthorizationAction(CardService.newAuthorizationAction()
        .setAuthorizationUrl(this.props.bitbucketAuthUrl))

    return CardService.newCardSection()
      .addWidget(createKeyValueWidget({
        content: 'Bitbucket',
        icon: Icons.lock,
      }))
      .addWidget(createKeyValueWidget({
        label: `${owner} / ${repo}`,
        content: [
          'Connect to Bitbucket for full details',
          colorString(key, Colors.default),
        ].join('<br>'),
        multiline: true,
      }))
      .addWidget(authButton)
  }
}
