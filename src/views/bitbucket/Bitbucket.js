import { get } from 'lodash'

import View from '../View'

export default class extends View {
  get product () {
    return 'Bitbucket'
  }

  _getEntityLink (entity) {
    return get(entity, 'links.html.href')
  }
}
