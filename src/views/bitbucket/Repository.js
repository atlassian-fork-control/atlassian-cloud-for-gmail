import BitbucketView from './Bitbucket'
import Icons from '../Icons'
import {
  createKeyValueWidget,
  formatDateTime
} from '../utils'
import widgets from './widgets'

export default class extends BitbucketView {
  get url () {
    return this._getEntityLink(this.props.repo)
  }

  get header () {
    const { repo } = this.props
    const avatarUrl = repo.is_private ? Icons.repository : repo.links.avatar.href

    return CardService.newCardHeader()
      .setTitle(repo.name)
      .setImageStyle(CardService.ImageStyle.CIRCLE)
      .setImageUrl(avatarUrl)
  }

  get sections () {
    const { repo } = this.props

    return CardService.newCardSection()
      .addWidget(widgets.account(repo.owner, 'Owner'))
      .addWidget(createKeyValueWidget({
        label: 'SCM',
        icon: Icons.branch,
        content: repo.scm,
      }))
      .addWidget(createKeyValueWidget({
        label: 'Language',
        icon: Icons.source,
        content: repo.language,
      }))
      .addWidget(createKeyValueWidget({
        label: 'Created',
        icon: Icons.calendar,
        content: formatDateTime(repo.created_on),
      }))
      .addWidget(createKeyValueWidget({
        label: 'Created',
        icon: Icons.calendar,
        content: formatDateTime(repo.updated_on),
      }))
  }
}
