import { formatHtml } from '../../../views/utils'
import BitbucketIssueView from './Issue'

export default class extends BitbucketIssueView {
  get sections () {
    const { issue } = this.props
    const html = formatHtml({
      html: issue.content.html,
      defaultValue: 'No description.',
      truncate: false,
    })

    return CardService.newCardSection()
      .setHeader('Description')
      .addWidget(CardService.newTextParagraph().setText(html))
  }
}
