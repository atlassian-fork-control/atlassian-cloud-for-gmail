import BitbucketIssueView from './Issue'
import widgets from '../widgets'

export default class extends BitbucketIssueView {
  get url () {
    return this._getEntityLink(this.props.comment)
  }

  get sections () {
    return CardService.newCardSection()
      .setHeader('<b>Comment</b>')
      .addWidget(widgets.comment({
        comment: this.props.comment,
        truncate: false,
      }))
  }

  setProductLinkButtonSection () {
    return super.setProductLinkButtonSection(`View Comment In ${this.product}`)
  }
}
