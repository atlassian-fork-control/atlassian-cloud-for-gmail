import View from '../../View'
import Icons from '../../Icons'
import { createActionButtonWidget } from '../../utils'

export default class extends View {
  get header () {
    return CardService.newCardHeader()
      .setImageUrl(Icons.edit)
      .setTitle('Edit issue')
  }

  get sections () {
    return CardService.newCardSection()
      .addWidget(this._getTitleInput())
      .addWidget(this._getKindSelect())
      .addWidget(this._getPrioritySelect())
      .addWidget(this._getDescriptionInput())
      .addWidget(this._getButtons())
  }

  _getTitleInput () {
    return CardService.newTextInput()
      .setFieldName('title')
      .setTitle('Title')
      .setHint('Required field')
      .setMultiline(true)
      .setValue(this.props.issue.title)
  }

  _getKindSelect () {
    const selectionInput = CardService.newSelectionInput()
      .setType(CardService.SelectionInputType.DROPDOWN)
      .setTitle('Kind')
      .setFieldName('kind')

    const kinds = ['bug', 'enhancement', 'proposal', 'task']
    kinds.forEach(k => selectionInput.addItem(k, k, k === this.props.issue.kind))

    return selectionInput
  }

  _getPrioritySelect () {
    const selectionInput = CardService.newSelectionInput()
      .setType(CardService.SelectionInputType.DROPDOWN)
      .setTitle('Priority')
      .setFieldName('priority')

    const priorities = ['trivial', 'minor', 'major', 'critical', 'blocker']
    priorities.forEach(p => selectionInput.addItem(p, p, p === this.props.issue.priority))

    return selectionInput
  }

  _getDescriptionInput () {
    return CardService.newTextInput()
      .setFieldName('content')
      .setTitle('Description')
      .setMultiline(true)
      .setValue(this.props.issue.content.raw || '')
  }

  _getButtons () {
    const { issue } = this.props

    return CardService.newButtonSet()
      .addButton(createActionButtonWidget('Save Changes', 'bitbucket.issue.editForm.Save', {
        owner: issue.repository.owner.username,
        repo: issue.repository.name,
        id: issue.id,
      }))
      .addButton(createActionButtonWidget('Cancel', 'addon.NavigateBack'))
  }
}
