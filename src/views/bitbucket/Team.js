import BitbucketView from './Bitbucket'

export default class extends BitbucketView {
  get url () {
    return this._getEntityLink(this.props.team)
  }

  get header () {
    const { team } = this.props

    return CardService.newCardHeader()
      .setImageUrl(team.links.avatar.href)
      .setTitle(team.display_name || '')
  }
}
