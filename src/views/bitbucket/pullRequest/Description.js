import { formatHtml } from '../../../views/utils'
import PullRequestView from './PullRequest'

export default class extends PullRequestView {
  get sections () {
    const { summary } = this.props.pullRequest
    const html = formatHtml({
      html: summary.html,
      defaultValue: 'No description.',
      truncate: false,
    })

    return CardService.newCardSection()
      .setHeader('Description')
      .addWidget(CardService.newTextParagraph().setText(html))
  }
}
