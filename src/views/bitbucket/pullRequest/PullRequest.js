import { sortBy } from 'lodash'
import BitbucketView from '../Bitbucket'
import { BuildIcons } from '../../Icons'

import {
  createAction,
  createKeyValueWidget,
  createActionButtonWidget,
  formatDateTime,
  capitalizeString,
  createExternalLink,
  colorString,
  formatHtml
} from '../../utils'
import Colors, { pickStatusColor } from '../../Colors'
import widgets from '../widgets'

export default class extends BitbucketView {
  get url () {
    return this._getEntityLink(this.props.pullRequest)
  }

  get header () {
    const { title, id } = this.props.pullRequest

    return CardService.newCardHeader()
      .setTitle(title)
      .setSubtitle(`Pull request #${id}`)
  }

  get sections () {
    return [
      this._getDetailSection(),
      this._getReviewersSection(),
      this._getBuildsSection(),
      this._getCommentsSection(),
    ]
  }

  _getDetailSection () {
    const { pullRequest } = this.props

    const cardSection = CardService.newCardSection()
      .addWidget(createKeyValueWidget({
        label: 'Repository',
        content: pullRequest.destination.repository.full_name,
        icon: pullRequest.destination.repository.links.avatar.href,
      }))
      .addWidget(widgets.basic('Status', colorString(capitalizeString(pullRequest.state), pickStatusColor(
        pullRequest.state,
        'bitbucket',
        'pullRequest'
      ))))
      .addWidget(widgets.basic('Source', pullRequest.source.branch.name))
      .addWidget(widgets.basic('Destination', pullRequest.destination.branch.name))
      .addWidget(createKeyValueWidget({
        label: 'Description',
        content: formatHtml({
          html: pullRequest.summary.html,
          defaultValue: 'No description.',
        }),
        multiline: true,
        action: createAction('bitbucket.pullRequest.ShowDescription', { pullRequest }),
      }))
      .addWidget(widgets.account(pullRequest.author, 'Author'))
      .addWidget(widgets.basic('Last updated', formatDateTime(pullRequest.updated_on)))

    if (pullRequest.state === 'OPEN') {
      cardSection.addWidget(createActionButtonWidget('Merge', 'bitbucket.pullRequest.merge.Show', { pullRequest }))
    }

    return cardSection
  }

  _getReviewersSection () {
    const { pullRequest, currentUser } = this.props
    const reviewers = pullRequest.participants.filter(p => p.role === 'REVIEWER')

    if (!reviewers.length) return null

    const cardSection = CardService.newCardSection().setHeader('<b>Reviewers</b>')
    const approvers = reviewers.filter(r => r.approved)
    const others = reviewers.filter(r => !r.approved)
    const currentUserFirst = arr => sortBy(arr, item => (item.user.uuid !== currentUser.uuid))
    const sortedReviewers = [...currentUserFirst(approvers), ...currentUserFirst(others)]

    sortedReviewers.forEach((r) => {
      cardSection.addWidget(createKeyValueWidget({
        content: r.user.display_name,
        icon: r.user.links.avatar.href,
        action: createAction('bitbucket.user.Show', { username: r.user.username }),
        bottomLabel: r.approved && `Approved ${formatDateTime(r.participated_on)}`,
      }))
    })

    return cardSection
  }

  _getBuildsSection () {
    const { statuses } = this.props

    if (!statuses.values.length) return null

    const cardSection = CardService.newCardSection()
      .setHeader('<b>Builds</b>')
    // Is it a pipeline, or a build result from another tool
    const matchRes = url => /https:\/\/bitbucket.org\/([^/]+)\/([^/]+)\/addon\/pipelines\/home#!\/results\/([^/]+)/.exec(url)
    const statusAction = (owner, repo, id) => createAction('bitbucket.pipeline.Show', { owner, repo, id })

    statuses.values.forEach(({ description, url, key, state, refname }) => {
      const widgetData = {
        icon: BuildIcons[state],
        content: refname || key,
        bottomLabel: description,
      }

      const res = matchRes(url)
      if (res) {
        const [, owner, repo, id] = res
        Object.assign(widgetData, {
          action: statusAction(owner, repo, id),
          bottomLabel: `Pipeline #${id}`,
        })
      } else {
        Object.assign(widgetData, { openLink: createExternalLink(url) })
      }

      cardSection.addWidget(createKeyValueWidget(widgetData))
    })

    return cardSection
  }

  _getCommentsSection () {
    const { pullRequest, comments } = this.props
    const { pagelen, size, values: commentsValues } = comments

    const cardSection = CardService.newCardSection()

    if (size > 3) { // leaves exactly 3 top comments
      cardSection
        .setCollapsible(true)
        .setNumUncollapsibleWidgets(5) // cuz input + button + 3 comments
    }

    cardSection
      .addWidget(widgets.commentInput())
      .addWidget(createActionButtonWidget('Save Comment', 'bitbucket.pullRequest.comment.Create', {
        owner: pullRequest.destination.repository.owner.username,
        repo: pullRequest.destination.repository.name,
        id: pullRequest.id,
      }))

    commentsValues.forEach((comment) => {
      const action = createAction('bitbucket.pullRequest.comment.Show', { comment, pullRequest })
      cardSection.addWidget(widgets.comment({ comment, action }))
    })

    if (size > pagelen) {
      cardSection
        .addWidget(createKeyValueWidget({
          content: colorString(`Show <b>${size}</b> comments in Bitbucket`, Colors.blue),
          openLink: createExternalLink(this.url),
        }))
    }

    return cardSection
  }
}
