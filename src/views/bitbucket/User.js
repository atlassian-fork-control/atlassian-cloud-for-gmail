import Icons from '../Icons'
import BitbucketView from './Bitbucket'

import {
  createKeyValueWidget,
  formatDateTime
} from '../utils'

export default class extends BitbucketView {
  get url () {
    return this._getEntityLink(this.props.user)
  }

  get header () {
    const { user } = this.props

    return CardService.newCardHeader()
      .setImageUrl(user.links.avatar.href)
      .setTitle(user.display_name || '')
  }

  get sections () {
    const { user } = this.props

    return CardService.newCardSection()
      .addWidget(createKeyValueWidget({
        content: user.username,
        icon: Icons.unassigned,
      }))
      .addWidget(createKeyValueWidget({
        content: `Member since ${formatDateTime(user.created_on)}`,
        icon: Icons.time,
      }))
  }
}
