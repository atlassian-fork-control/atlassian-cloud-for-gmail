import BitbucketView from './Bitbucket'
import widgets from './widgets'
import {
  capitalizeString,
  createKeyValueWidget,
  formatDateTime,
  formatDuration,
  createActionButtonWidget,
  colorString
} from '../utils'
import { pickStatusColor } from '../Colors'
import { BuildIcons } from '../Icons'

const RerunStates = ['ERROR', 'EXPIRED', 'FAILED', 'SKIPPED', 'STOPPED']

export default class extends BitbucketView {
  get url () {
    const { repository, build_number: buildNumber } = this.props.pipeline

    return `${this._getEntityLink(repository)}/addon/pipelines/home#!/results/${buildNumber}`
  }

  get header () {
    const { pipeline } = this.props

    return CardService.newCardHeader()
      .setTitle(`${pipeline.target.commit.message}`)
      .setSubtitle(`Pipeline #${pipeline.build_number}`)
  }

  get sections () {
    const { pipeline } = this.props
    const { creator, target, repository } = pipeline

    const state = this._getBuildState()
    const statusWidget = createKeyValueWidget({
      label: 'Status',
      icon: BuildIcons[state],
      content: colorString(capitalizeString(state), pickStatusColor(
        state,
        'bitbucket',
        'pipeline'
      )),
    })

    if (RerunStates.includes(state)) {
      statusWidget.setButton(this._getRerunButton())
    }

    const section = CardService.newCardSection()
      .addWidget(createKeyValueWidget({
        label: 'Repository',
        icon: repository.links.avatar.href,
        content: repository.full_name,
      }))
      .addWidget(statusWidget)
      .addWidget(widgets.account(creator, 'Push by'))
      .addWidget(widgets.basic('Duration', formatDuration(pipeline.duration_in_seconds)))
      .addWidget(widgets.basic('Started', formatDateTime(pipeline.created_on)))

    if (target.ref_type === 'branch') {
      section
        .addWidget(createKeyValueWidget({
          label: 'Commit',
          content: target.commit.hash.substr(0, 7),
        }))
        .addWidget(createKeyValueWidget({
          label: 'Branch',
          content: target.ref_name,
        }))
    }

    return section
  }

  _getBuildState () {
    const { pipeline: { state: { result, name } } } = this.props

    return result ? result.name : name
  }

  _getRerunButton () {
    const { repository, target } = this.props.pipeline

    return createActionButtonWidget(
      'Rerun',
      'bitbucket.pipeline.Rerun', {
        owner: repository.owner.uuid,
        repo: repository.uuid,
        commitHash: target.commit.hash,
        branchName: target.ref_name,
      }
    )
  }
}
