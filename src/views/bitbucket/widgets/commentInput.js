export default (fieldName = 'content', multiline = true) => CardService.newTextInput()
  .setTitle('Add a comment...')
  .setFieldName(fieldName)
  .setMultiline(multiline)
