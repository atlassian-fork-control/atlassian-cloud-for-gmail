import account from './account'
import basic from './basic'
import comment from './comment'
import commentInput from './commentInput'

export default {
  account,
  basic,
  comment,
  commentInput,
}
