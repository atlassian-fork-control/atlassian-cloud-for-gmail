import { createKeyValueWidget } from '../../utils'

export default (label = '', content = '-') => createKeyValueWidget({ label, content })
