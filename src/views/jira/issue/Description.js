import { formatHtml } from '../../../views/utils'
import JiraView from '../Jira'

export default class extends JiraView {
  get sections () {
    const { issue } = this.props
    const textParagraph = CardService.newTextParagraph()
      .setText(formatHtml({
        html: issue.renderedFields.description,
        defaultValue: 'No description.',
        truncate: false,
      }))

    return CardService.newCardSection()
      .setHeader('Description')
      .addWidget(textParagraph)
  }
}
