import View from '../../View'
import Icons from '../../Icons'
import { createAction, createActionButtonWidget, capitalizeString } from '../../utils'
import widgets from '../widgets'

export default class extends View {
  get header () {
    return CardService.newCardHeader()
      .setImageUrl(Icons.edit)
      .setTitle('Edit issue')
  }

  get sections () {
    const { instanceHost, issue, editableFields } = this.props

    const isEditable = fieldName => editableFields.includes(fieldName)

    const currentAssignee = widgets.user({
      user: issue.fields.assignee,
      label: 'Assignee',
      instanceHost,
    })

    const section = CardService.newCardSection()

    isEditable('summary') && section.addWidget(this._getSummaryInput())
    isEditable('priority') && section.addWidget(this._getPrioritySelect())
    isEditable('status') && section.addWidget(this._getStatusSelect())
    isEditable('assignee') && section
      .addWidget(currentAssignee)
      .addWidget(this._getAutocompleteInput())
    isEditable('description') && section.addWidget(this._getDescriptionInput())

    section.addWidget(this._getButtons())

    return section
  }

  _getSummaryInput () {
    return CardService.newTextInput()
      .setFieldName('summary')
      .setTitle('Summary')
      .setHint('Required field')
      .setMultiline(true)
      .setValue(this.props.issue.fields.summary)
  }

  _getStatusSelect () {
    const { issue } = this.props
    const { status: currentStatus } = issue.fields

    const selectionInput = CardService.newSelectionInput()
      .setType(CardService.SelectionInputType.DROPDOWN)
      .setTitle('Status')
      .setFieldName('transition')

    // There are custom workflows when API doesn't return the current status
    // in transitions list which basically means current status doesn't appear in UI.
    // and user is forced to change issue status while editing it.
    // So, in case API doesn't return current status in transitions list we still
    // render it (as selected value) to provide the most adequate user experience.

    if (issue.transitions.every(t => t.to.id !== currentStatus.id)) {
      selectionInput.addItem(capitalizeString(currentStatus.name), '', true)
    }

    issue.transitions.forEach((t) => {
      const isSelected = t.to.id === currentStatus.id
      selectionInput.addItem(capitalizeString(t.to.name), t.id, isSelected)
    })

    return selectionInput
  }

  _getAutocompleteInput () {
    const { instanceHost, issue } = this.props

    return CardService.newTextInput()
      .setFieldName('assigneeEmail')
      .setTitle('New assignee')
      .setHint('Enter email address or name')
      .setMultiline(true)
      .setSuggestionsAction(createAction('jira.user.Search', {
        instanceHost,
        projectId: issue.fields.project.id,
      }))
  }

  _getDescriptionInput () {
    return CardService.newTextInput()
      .setFieldName('description')
      .setTitle('Description')
      .setMultiline(true)
      .setValue(this.props.issue.fields.description || '')
  }

  _getPrioritySelect () {
    const { priorities, issue } = this.props

    const selectionInput = CardService.newSelectionInput()
      .setType(CardService.SelectionInputType.DROPDOWN)
      .setTitle('Priority')
      .setFieldName('priority')

    const currentPriority = issue.fields.priority.id

    priorities.forEach((p) => {
      const isSelected = p.id === currentPriority
      selectionInput.addItem(p.name, p.id, isSelected)
    })

    return selectionInput
  }

  _getButtons () {
    const { instanceHost, issue } = this.props

    return CardService.newButtonSet()
      .addButton(createActionButtonWidget('Save Changes', 'jira.issue.editForm.Save', {
        instanceHost,
        issueKey: issue.key,
      }))
      .addButton(createActionButtonWidget('Cancel', 'addon.NavigateBack'))
  }
}
