import JiraView from '../Jira'
import Icons, { getMimeTypeIcon } from '../../Icons'
import { createKeyValueWidget } from '../../utils'

export default class extends JiraView {
  get url () {
    return `${super.url}#attachmentmodule`
  }

  get sections () {
    const cardSection = CardService.newCardSection()
      .setHeader('<b>Attachments</b>')

    this.props.issue.fields.attachment.forEach((a) => {
      const widget = this._getAttachmentWidget(a)
      cardSection.addWidget(widget)
    })

    return cardSection
  }

  _getAttachmentWidget (attachment) {
    const imageButton = CardService.newImageButton()
      .setIconUrl(Icons.open)
      .setOpenLink(CardService.newOpenLink()
        .setUrl(attachment.content))

    return createKeyValueWidget({
      content: attachment.filename,
      icon: getMimeTypeIcon(attachment.mimeType),
      multiline: true,
    }).setButton(imageButton)
  }
}
