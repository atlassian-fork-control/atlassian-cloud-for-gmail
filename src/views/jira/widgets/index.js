import user from './user'
import comment from './comment'

export default {
  user,
  comment,
}
