import View from '../View'

export default class extends View {
  get url () {
    const { key } = this.props.issue

    return `${this.baseUrl}/browse/${key}`
  }

  get header () {
    const { issue } = this.props

    return CardService.newCardHeader()
      .setTitle(issue.fields.summary)
      .setSubtitle(issue.key)
  }

  get product () {
    return 'Jira'
  }

  get baseUrl () {
    return `https://${this.props.instanceHost}`
  }
}
