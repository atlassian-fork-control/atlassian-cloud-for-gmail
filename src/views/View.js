import { createLinkButtonWidget } from './utils'

export default class {
  constructor (props = {}) {
    this.props = props

    this._cardBuilder = CardService.newCardBuilder()
  }

  render () {
    return this
      .setHeader()
      .setProductLinkMenuAction()
      .setSections()
      .setProductLinkButtonSection()
      .build()
  }

  build () {
    return this._cardBuilder.build()
  }

  setHeader () {
    this._cardBuilder.setHeader(this.header)

    return this
  }

  setProductLinkMenuAction (text = `Open in ${this.product}`) {
    const { url, product } = this

    if (!url || !product) return this

    const universalAction = CardService.newCardAction()
      .setText(text)
      .setOpenLink(CardService.newOpenLink().setUrl(url))

    this._cardBuilder.addCardAction(universalAction)

    return this
  }

  setSections () {
    const { sections } = this
    const addSection = (section) => {
      if (section) this._cardBuilder.addSection(section)
    }

    Array.isArray(sections)
      ? sections.forEach(s => addSection(s))
      : addSection(sections)

    return this
  }

  setProductLinkButtonSection (text = `Open in ${this.product}`) {
    const { url, product } = this

    if (!url || !product) return this

    const productLinkSection = CardService.newCardSection()
      .addWidget(createLinkButtonWidget(text, url))

    this._cardBuilder.addSection(productLinkSection)

    return this
  }
}
