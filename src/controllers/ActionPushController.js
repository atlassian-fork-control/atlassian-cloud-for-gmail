import { get } from 'lodash'
import Controller from './Controller'
import { getNavigationActionResponse } from './responseWrappers'

/**
 * Pushes card to navigation history so button 'back'
 * is presented in header of the new card
 */
export default class extends Controller {
  navigateTo (view) {
    const card = view.render()
    // we can't use navigation response for getContextualAddon trigger
    return get(this.event, 'parameters.actionName')
      ? getNavigationActionResponse(card)
      : card
  }
}
