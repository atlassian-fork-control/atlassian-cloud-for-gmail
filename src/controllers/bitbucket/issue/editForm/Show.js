import ActionPushController from '../../../ActionPushController'
import IssueEditFormView from '../../../../views/bitbucket/issue/EditForm'

export default class extends ActionPushController {
  execute ({ issue }) {
    return new IssueEditFormView({ issue })
  }
}
