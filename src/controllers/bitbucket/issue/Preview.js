import ActionPushController from '../../ActionPushController'
import Bitbucket from '../../../common/net/Bitbucket'

export default class extends ActionPushController {
  execute ({ owner, repo, id }) {
    const fields = [
      'repository',
      'title',
      'id',
      'state',
      'updated_on',
    ].join(',')

    const issue = new Bitbucket(owner, repo).getIssue(id, { fields })

    return {
      source: issue.repository.name,
      title: issue.title,
      key: `Issue #${issue.id}`,
      status: issue.state,
      updatedAt: issue.updated_on,
    }
  }
}
