import ActionPushController from '../../ActionPushController'
import Bitbucket from '../../../common/net/Bitbucket'
import IssueView from '../../../views/bitbucket/issue/Issue'
import analytics from '../../../common/analytics'

export default class Show extends ActionPushController {
  execute ({ owner, repo, id }) {
    const fields = [
      'values.user.display_name',
      'values.created_on',
      'values.content.html',
      'size',
      'pagelen',
    ].join(',')

    const bitbucket = new Bitbucket(owner, repo)

    const [issue, comments] = bitbucket
      .parallel()
      .getIssue(id, { fields: '+repository.owner' })
      .getIssueComments(id, {
        pagelen: 10,
        sort: '-created_on',
        q: 'content.raw != null AND content.raw != ""',
        fields,
      })
      .fetch()

    const isWatching = this._getIssueWatchStatus(id, bitbucket)
    const attachmentsSize = this._getAttachmentsSize(id, bitbucket)

    analytics.pageView('Bitbucket Issue')

    return new IssueView({ issue, comments, attachmentsSize, isWatching })
  }

  _getIssueWatchStatus (issueId, bitbucket) {
    try {
      // We're good if it's just not failing
      bitbucket.getIssueWatchStatus(issueId)

      return true
    } catch (error) {
      // Actually this checks if 404 is being returned
      // If 204 -> OK (kinda watching)
      // If 404 -> NOT_FOUND (kinda not watching)
      if (error.code === 'NOT_FOUND') return false
      // In case of master issue tracker API returns 403
      // So we handle it gently by returning undefined and
      // hiding corresponding sections in view.
      // This approach applies for this method and _getAttachmentsSize
      if (error.code === 'FORBIDDEN') return

      throw error
    }
  }

  _getAttachmentsSize (issueId, bitbucket) {
    try {
      const { size } = bitbucket.getIssueAttachments(issueId, { fields: 'size' })

      return size
    } catch (error) {
      if (error.code === 'FORBIDDEN') return

      throw error
    }
  }
}
