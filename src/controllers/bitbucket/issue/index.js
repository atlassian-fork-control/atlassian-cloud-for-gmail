import Preview from './Preview'
import Show from './Show'
import ShowDescription from './ShowDescription'
import ShowAttachments from './ShowAttachments'
import Watch from './Watch'
import comment from './comment'
import editForm from './editForm'

export default {
  Preview,
  Show,
  ShowDescription,
  ShowAttachments,
  Watch,
  comment,
  editForm,
}
