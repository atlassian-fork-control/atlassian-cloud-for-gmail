import ActionController from '../../../ActionController'
import Bitbucket from '../../../../common/net/Bitbucket'
import ShowController from '../../issue/Show'
import analytics from '../../../../common/analytics'
import AddonError from '../../../../common/AddonError'

export default class extends ActionController {
  execute ({ owner, repo, id, content }) {
    if (!content) {
      throw new AddonError('Empty comments are not allowed', 'VALIDATION_ERROR', 'BITBUCKET')
    }

    new Bitbucket(owner, repo).createIssueComment(id, content)

    analytics.event({
      category: 'Bitbucket Issue',
      action: 'Create comment',
    })

    return new ShowController({ event: this.event }).execute({ owner, repo, id })
  }
}
