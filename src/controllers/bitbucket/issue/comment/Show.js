import ActionPushController from '../../../ActionPushController'
import IssueCommentView from '../../../../views/bitbucket/issue/Comment'

export default class extends ActionPushController {
  execute ({ issue, comment }) {
    return new IssueCommentView({ issue, comment })
  }
}
