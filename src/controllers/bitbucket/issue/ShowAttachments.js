import ActionPushController from '../../ActionPushController'
import Bitbucket from '../../../common/net/Bitbucket'
import IssueAttachmentsView from '../../../views/bitbucket/issue/Attachments'
import analytics from '../../../common/analytics'

export default class Show extends ActionPushController {
  execute ({ issue }) {
    const { id, repository } = issue

    const attachments = new Bitbucket(repository.owner.username, repository.name)
      .getIssueAttachments(id)

    analytics.event({
      category: 'Bitbucket Issue',
      action: 'Show attachments',
    })

    return new IssueAttachmentsView({ attachments, issue })
  }
}
