import pipeline from './pipeline'
import pullRequest from './pullRequest'
import issue from './issue'
import team from './team'
import user from './user'

export default {
  pipeline,
  pullRequest,
  issue,
  team,
  user,
}
