import ActionPushController from '../../../ActionPushController'
import PullRequestMergeView from '../../../../views/bitbucket/pullRequest/Merge'

export default class extends ActionPushController {
  execute ({ pullRequest }) {
    return new PullRequestMergeView({ pullRequest })
  }
}
