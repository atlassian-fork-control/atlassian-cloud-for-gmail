import ActionPushController from '../../ActionPushController'
import Bitbucket from '../../../common/net/Bitbucket'

export default class extends ActionPushController {
  execute ({ owner, repo, id }) {
    const fields = [
      'destination.repository.name',
      'title',
      'id',
      'state',
      'updated_on',
    ].join(',')

    const pullRequest = new Bitbucket(owner, repo).getPullRequest(id, { fields })

    return {
      source: pullRequest.destination.repository.name,
      title: pullRequest.title,
      key: `Pull Request #${pullRequest.id}`,
      status: pullRequest.state,
      updatedAt: pullRequest.updated_on,
    }
  }
}
