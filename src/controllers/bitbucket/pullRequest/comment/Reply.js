import ActionController from '../../../ActionController'
import Bitbucket from '../../../../common/net/Bitbucket'
import AddonError from '../../../../common/AddonError'
import ShowPullRequest from '../Show'
import analytics from '../../../../common/analytics'

export default class extends ActionController {
  execute ({ pullRequest, comment, content }) {
    if (!content) {
      throw new AddonError('Empty comments are not allowed', 'VALIDATION_ERROR', 'BITBUCKET')
    }

    const { id: parentCommentId } = comment
    const { id, destination: { repository } } = pullRequest
    const owner = repository.owner.username
    const repo = repository.name

    new Bitbucket(owner, repo).createPullRequestComment(id, { content, parentCommentId })

    analytics.event({
      category: 'Bitbucket Pull Request',
      action: 'Reply to a comment',
    })

    return new ShowPullRequest({ event: this.event })
      .execute({ owner, repo, id })
  }
}
