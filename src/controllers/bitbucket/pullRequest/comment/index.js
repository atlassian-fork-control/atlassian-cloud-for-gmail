import Create from './Create'
import Reply from './Reply'
import Show from './Show'

export default {
  Create,
  Reply,
  Show,
}
