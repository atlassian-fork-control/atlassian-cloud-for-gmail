import addon from './addon'
import bitbucket from './bitbucket'
import jira from './jira'
import OAuthCallback from './addon/OAuthCallback'

export default {
  addon,
  bitbucket,
  jira,
  OAuthCallback,
}
