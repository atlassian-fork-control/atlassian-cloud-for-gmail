import Controller from '../Controller'
import Jira from '../../common/net/Jira'
import Bitbucket from '../../common/net/Bitbucket'
import analytics from '../../common/analytics'
import ShowSettings from './ShowSettings'

export default class extends Controller {
  execute ({ product }) {
    const ProductApiClass = { Jira, Bitbucket }[product]

    if (!ProductApiClass) {
      throw new Error(`Unknown product ${product}`)
    }

    analytics.event({
      category: 'Disconnect',
      action: `product: ${product}`,
    })

    ProductApiClass.oauthService().reset()
    return new ShowSettings({ event: this.event }).execute()
  }

  navigateTo (view) {
    // workaround to invalidate all the possible client side cache with setStateChanged
    return CardService.newActionResponseBuilder()
      .setNavigation(CardService.newNavigation().updateCard(view.render()))
      .setStateChanged(true)
      .build()
  }
}
