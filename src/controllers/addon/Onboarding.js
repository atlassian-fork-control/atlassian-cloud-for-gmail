import ActionController from '../ActionController'
import { userProperties } from '../../common/properties'
import ShowController from './Show'

export default class extends ActionController {
  execute ({ onboardingState }) {
    userProperties.set('onboardingState', onboardingState)

    return new ShowController({ event: this.event }).execute()
  }
}
