import ActionPushController from '../ActionPushController'
import AlertView from '../../views/addon/Alert'

export default class extends ActionPushController {
  execute ({ type, text }) {
    return new AlertView({ type, text })
  }
}
