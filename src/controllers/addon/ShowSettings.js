import UniversalActionController from '../UniversalActionController'
import Bitbucket from '../../common/net/Bitbucket'
import Jira from '../../common/net/Jira'
import analytics from '../../common/analytics'
import SettingsView from '../../views/addon/Settings'

export default class extends UniversalActionController {
  execute () {
    const bitbucketUser = this._getCurrentBitbucketUser()
    const jiraUser = this._getCurrentJiraUser()

    const bitbucketAuthUrl = Bitbucket.oauthService().getAuthorizationUrl()
    const jiraAuthUrl = Jira.oauthService().getAuthorizationUrl()

    analytics.pageView('Settings')

    return new SettingsView({
      bitbucketUser,
      jiraUser,
      bitbucketAuthUrl,
      jiraAuthUrl,
    })
  }

  _getCurrentBitbucketUser () {
    try {
      const [bitbucketUser, emails] = new Bitbucket()
        .parallel()
        .getCurrentUser()
        .getCurrentUserEmails()
        .fetch()

      const primaryEmail = emails.values.find(e => e.is_primary)
      bitbucketUser.email = primaryEmail && primaryEmail.email

      return bitbucketUser
    } catch (error) {
      if (error.code === 'UNAUTHORIZED') {
        return
      }

      throw error
    }
  }

  _getCurrentJiraUser () {
    try {
      const jira = new Jira()

      const [jiraUser, resources] = jira
        .parallel()
        .getCurrentUser()
        .getAccessibleResources()
        .fetch()

      // User can revoke access to all the Jira sites but his access token will still be valid
      // So we need an additional check to know if he has any sites in accessible resources
      if (!resources.length) return

      jira.parallel()
      resources.forEach(resource => jira.getServerInfo({ cloudId: resource.id }))

      jiraUser.sites = jira.fetch()

      return jiraUser
    } catch (error) {
      if (error.code === 'UNAUTHORIZED') {
        return
      }

      throw error
    }
  }
}
