import Controller from '../Controller'
import Jira from '../../common/net/Jira'
import Bitbucket from '../../common/net/Bitbucket'
import analytics from '../../common/analytics'
import { userProperties } from '../../common/properties'

export default class extends Controller {
  execute ({ response, product }) {
    try {
      let isAuthorized
      if (product === 'jira') isAuthorized = Jira.oauthService().handleCallback(response)
      if (product === 'bitbucket') isAuthorized = Bitbucket.oauthService().handleCallback(response)

      const onboardingState = userProperties.get('onboardingState')

      if (onboardingState !== 'done') {
        if (!product || product === 'jira') userProperties.set('onboardingState', 'bitbucket')
        if (product === 'bitbucket') userProperties.set('onboardingState', 'done')
      }

      if (!isAuthorized) throw new Error('Access Denied')

      analytics.event({
        category: 'Connect',
        action: `product: ${product}`,
      })

      return HtmlService.createHtmlOutputFromFile('static/auth-success')
    } catch (error) {
      const template = HtmlService.createTemplateFromFile('static/auth-failure')
      template.errorMessage = error.toString()
      return template.evaluate()
    }
  }

  navigateTo (html) {
    return html
  }
}
