// TODO: use regexp named groups when they'll become available in Babel
// https://github.com/tc39/proposal-regexp-named-groups
export default [
  {
    product: 'bitbucket',
    name: 'pullRequest',
    regexp: /https:\/\/bitbucket\.org\/([^/]+?)\/([^/]+?)\/pull-requests?\/(\d+)/i,
    mapGroups: ([, owner, repo, id]) => ({ owner, repo, id }),
  },
  {
    product: 'bitbucket',
    name: 'pipeline',
    regexp: /https:\/\/bitbucket\.org\/([^/]+?)\/([^/]+?)\/addon\/pipelines\/home#!\/results\/([^/&?]+)/i,
    mapGroups: ([, owner, repo, id]) => ({ owner, repo, id }),
  },
  {
    product: 'bitbucket',
    name: 'issue',
    regexp: /https:\/\/bitbucket.org\/([^/]+?)\/([^/]+?)\/issues?\/(\d+)/i,
    mapGroups: ([, owner, repo, id]) => ({ owner, repo, id }),
  },
  {
    product: 'jira',
    name: 'issue',
    regexp: /https:\/\/([-0-9a-z]+\.(atlassian\.net|jira\.com))\/(browse|projects\/[0-9A-Z]+\/issues)\/([0-9A-Z]+-\d+)/i,
    mapGroups: ([, instanceHost,,, issueKey]) => ({ instanceHost, issueKey }),
  },
  {
    product: 'jira',
    name: 'issue',
    regexp: /https:\/\/([-0-9a-z]+\.(atlassian\.net|jira\.com))\/(servicedesk\/customer\/portal|projects\/[0-9A-Z]+\/queues\/custom)\/\d+\/([0-9A-Z]+-\d+)/i,
    mapGroups: ([, instanceHost,,, issueKey]) => ({ instanceHost, issueKey }),
  },
]
