import { orderBy } from 'lodash'
import Controller from '../Controller'
import Jira from '../../common/net/Jira'
import LinkComposeView from '../../views/addon/LinkCompose'

export default class extends Controller {
  execute ({ issueQuery = '' }) {
    const issues = this._searchJiraIssues(issueQuery)

    return new LinkComposeView({ issues })
  }

  navigateTo (view) {
    return view.render()
  }

  _searchJiraIssues (query) {
    const jql = this._getJql(query)

    const jira = new Jira()
    const accessibleResources = jira.getAccessibleResources()

    jira.parallel()
    accessibleResources.forEach(r => jira.searchIssues({
      jql,
      fields: 'summary,issuetype,project,lastViewed',
      maxResults: 10,
      validateQuery: 'none',
    }, { cloudId: r.id }))

    const issues = jira.fetch().flatMap(response => response.issues)

    // restore order in case multiple resources connected
    return orderBy(issues, i => new Date(i.fields.lastViewed), 'desc').slice(0, 10)
  }

  _getJql (query) {
    let jql = ''

    if (query) {
      jql = `summary ~ "${query}*" `

      if (/^[a-zA-Z]+$/.test(query)) {
        // if the search term could be a project key, prepend a project clause
        jql += `OR project = "${query}" `
      } else if (/^[a-zA-Z]+-\d+$/.test(query)) {
        // else if the search term could be an issue key, prepend an issue clause
        jql += `OR key = "${query}" `
      }
    }

    return `${jql} ORDER BY lastViewed`
  }
}
