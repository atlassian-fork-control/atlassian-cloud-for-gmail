export function getNavigationActionResponse (card, push = true) {
  const nav = CardService.newNavigation()

  push
    ? nav.pushCard(card)
    : nav.popCard().updateCard(card)

  return CardService.newActionResponseBuilder()
    .setNavigation(nav)
    .build()
}

export function getUniversalActionResponse (card) {
  return CardService.newUniversalActionResponseBuilder()
    .displayAddOnCards([card])
    .build()
}

export function getUpdateDraftActionResponse (html) {
  return CardService.newUpdateDraftActionResponseBuilder()
    .setUpdateDraftBodyAction(CardService.newUpdateDraftBodyAction()
      .addUpdateContent(html, CardService.ContentType.IMMUTABLE_HTML)
      .setUpdateType(CardService.UpdateDraftBodyType.IN_PLACE_INSERT))
    .build()
}

export function getSuggestionResponse (suggestionsStrings) {
  const suggestions = CardService.newSuggestions().addSuggestions(suggestionsStrings)

  return CardService.newSuggestionsResponseBuilder()
    .setSuggestions(suggestions)
    .build()
}
