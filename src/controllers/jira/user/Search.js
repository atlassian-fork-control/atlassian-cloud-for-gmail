import SuggestionsController from '../../SuggestionsController'
import Jira from '../../../common/net/Jira'

export default class extends SuggestionsController {
  execute ({ instanceHost, projectId, assigneeEmail }) {
    const users = new Jira(instanceHost).searchAssignableUsers({
      project: projectId,
      username: assigneeEmail,
      maxResults: 10,
    })

    return [
      'Unassigned',
      ...users.map(u => u.emailAddress),
    ]
  }
}
