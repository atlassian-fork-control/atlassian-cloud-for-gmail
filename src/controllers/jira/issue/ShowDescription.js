import ActionPushController from '../../ActionPushController'
import Jira from '../../../common/net/Jira'
import IssueDescriptionView from '../../../views/jira/issue/Description'
import analytics from '../../../common/analytics'

export default class extends ActionPushController {
  execute ({ instanceHost, issueKey }) {
    const issue = new Jira(instanceHost).getIssue(issueKey, {
      fields: 'description,summary',
      expand: 'renderedFields',
    })

    analytics.event({
      category: 'Jira Issue',
      action: 'Show description',
    })

    return new IssueDescriptionView({
      instanceHost,
      issue,
    })
  }
}
