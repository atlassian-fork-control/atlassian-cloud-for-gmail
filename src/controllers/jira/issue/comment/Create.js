import ActionController from '../../../ActionController'
import Jira from '../../../../common/net/Jira'
import ShowController from '../../issue/Show'
import analytics from '../../../../common/analytics'
import AddonError from '../../../../common/AddonError'

export default class extends ActionController {
  execute ({ instanceHost, issueKey, newComment }) {
    if (!newComment) {
      throw new AddonError('Empty comments are not allowed', 'VALIDATION_ERROR', 'JIRA')
    }

    new Jira(instanceHost).createIssueComment(issueKey, { body: newComment })

    analytics.event({
      category: 'Jira Issue',
      action: 'Create comment',
    })

    return new ShowController({ event: this.event }).execute({ instanceHost, issueKey })
  }
}
