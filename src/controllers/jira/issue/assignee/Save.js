import ActionPopController from '../../../ActionPopController'
import Jira from '../../../../common/net/Jira'
import analytics from '../../../../common/analytics'
import ShowIssue from '../Show'
import { findUserByEmail } from '../utils'

export default class extends ActionPopController {
  execute ({ instanceHost, issueKey, assigneeEmail }) {
    const jira = new Jira(instanceHost)
    const assignee = findUserByEmail(instanceHost, assigneeEmail)

    jira.assignIssue(issueKey, assignee)

    analytics.event({
      category: 'Jira Issue',
      action: 'Assign',
    })

    return new ShowIssue({ event: this.event }).execute({ instanceHost, issueKey })
  }
}
