import ActionPushController from '../../ActionPushController'
import Jira from '../../../common/net/Jira'
import IssueAttachmentsView from '../../../views/jira/issue/Attachments'
import analytics from '../../../common/analytics'

export default class Show extends ActionPushController {
  execute ({ instanceHost, issueKey }) {
    const issue = new Jira(instanceHost).getIssue(issueKey, {
      fields: 'summary,attachment',
    })

    analytics.event({
      category: 'Jira Issue',
      action: 'Show attachment',
    })

    return new IssueAttachmentsView({
      instanceHost,
      issue,
    })
  }
}
