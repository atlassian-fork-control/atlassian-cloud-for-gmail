import ActionController from '../../ActionController'
import Jira from '../../../common/net/Jira'
import ShowController from './Show'
import analytics from '../../../common/analytics'

export default class extends ActionController {
  execute ({ instanceHost, issueKey, watch, accountId }) {
    const jira = new Jira(instanceHost)

    watch
      ? jira.addWatcher(issueKey, accountId)
      : jira.removeWatcher(issueKey, { accountId })

    analytics.event({
      category: 'Jira Issue',
      action: watch ? 'Watch' : 'Stop watching',
    })

    return new ShowController({ event: this.event }).execute({ instanceHost, issueKey })
  }
}
