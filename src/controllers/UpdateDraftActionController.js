import Controller from './Controller'
import { getUpdateDraftActionResponse } from './responseWrappers'

export default class extends Controller {
  navigateTo (html) {
    return getUpdateDraftActionResponse(html)
  }
}
