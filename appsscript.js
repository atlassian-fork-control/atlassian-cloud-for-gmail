const environment = process.env.NODE_ENV || 'local'

const name = {
  production: 'Atlassian',
  test: 'Atlassian (Test)',
  local: `Atlassian (${process.env.USER})`,
}[environment]

const logoUrl = {
  production: 'https://partner-engineering-cdn.atl-paas.net/atlassian-logo-blue.png',
  test: 'https://partner-engineering-cdn.atl-paas.net/atlassian-logo-blue-test.png',
  local: 'https://partner-engineering-cdn.atl-paas.net/atlassian-logo-blue-local.png',
}[environment]

module.exports = {
  timeZone: 'UTC',
  dependencies: {
    libraries: [{
      userSymbol: 'OAuth2',
      libraryId: '1B7FSrk5Zi6L1rSxxTDgDEUsPzlukDsi4KGuTMorsTQHhGBzBkMun4iDF',
      version: '28',
    }],
  },
  exceptionLogging: 'CLOUD',
  oauthScopes: [
    'https://www.googleapis.com/auth/script.storage',
    'https://www.googleapis.com/auth/script.external_request',
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/gmail.addons.execute',
    'https://www.googleapis.com/auth/gmail.addons.current.message.readonly',
    'https://www.googleapis.com/auth/gmail.addons.current.action.compose',
  ],
  gmail: {
    version: 'TRUSTED_TESTER_V2',
    name,
    logoUrl,
    contextualTriggers: [{
      unconditional: {},
      onTriggerFunction: 'getContextualAddOn',
    }],
    composeTrigger: {
      selectActions: [{
        text: 'Insert Atlassian link',
        runFunction: 'getComposeAddOn',
      }],
    },
    universalActions: [{
      text: 'Give feedback',
      runFunction: 'getFeedback',
    }, {
      text: 'Help',
      openLink: 'https://confluence.atlassian.com/cloud/atlassian-cloud-for-gmail-953138538.html',
    }, {
      text: 'Settings',
      runFunction: 'getSettings',
    }],
    openLinkUrlPrefixes: [
      '*',
    ],
    primaryColor: '#0052CC',
    secondaryColor: '#3B78E7',
  },
  urlFetchWhitelist: [
    'https://www.google-analytics.com/',
    'https://api.bitbucket.org/',
    'https://bitbucket.org/',
    'https://avatar-cdn.atlassian.com/',
    'https://bitbucket-assetroot.s3.amazonaws.com/',
    'https://api.stg.atlassian.com/',
    'https://api.atlassian.com/',
    'https://auth.stg.atlassian.com/',
    'https://accounts.atlassian.com/',
    'https://mgas.prod.public.atl-paas.net/',
  ],
}
