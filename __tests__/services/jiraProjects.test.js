import jiraProjects from 'src/services/jiraProjects'
import Jira from 'src/common/net/Jira'
import { userProperties } from 'src/common/properties'
import accessibleResources from '../helpers/mocks/responses/jira/getAccessibleResources.json'
import serverInfo from '../helpers/mocks/responses/jira/getServerInfo.json'
import projects from '../helpers/mocks/responses/jira/getProjects.json'

jest.mock('src/common/net/Jira')
jest.mock('src/common/properties')

const getAccessibleResources = jest.fn().mockReturnValue(accessibleResources)
const getServerInfo = jest.fn().mockReturnThis()
const getProjects = jest.fn().mockReturnValue(projects)
const parallel = jest.fn()
const fetch = jest.fn().mockReturnValue([serverInfo])

Jira.mockImplementation(() => ({
  getAccessibleResources,
  getServerInfo,
  getProjects,
  parallel,
  fetch,
}))

describe('Service jiraProjects', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('sync()', () => {
    it('Should call correct Jira methods', () => {
      jiraProjects.sync()

      expect(Jira).toHaveBeenCalledTimes(2)
      expect(getAccessibleResources).toBeCalled()
      expect(getServerInfo).toBeCalledWith({ cloudId: 'DUMMY-12345-678' })
      expect(Jira.mock.calls[1]).toEqual([null, 'DUMMY-12345-678'])

      expect(parallel).toBeCalled()
      expect(fetch).toBeCalled()
    })

    it('Should store project keys to userProperties', () => {
      jiraProjects.sync()
      expect(userProperties.set.mock.calls[0]).toEqual([
        'jiraStoreChunk-0',
        '{"syncedAt":1530000000000,"sites":[{"projectsKeys":["DEV","ST"],"instanceHost":"test-jira.atlassian.net"}]}',
      ])
      expect(userProperties.set.mock.calls[1]).toEqual(['jiraStoreMeta', { chunksReferences: ['jiraStoreChunk-0'] }])
    })

    it('Should NOT sync when previously synced less than 24 hrs ago', () => {
      userProperties.get
        .mockReturnValueOnce({ chunksReferences: ['dummy'] })
        .mockReturnValueOnce(`{"syncedAt": ${Date.now() - 23 * 60 * 60 * 1000}}`) // 23 hrs ago

      jiraProjects.sync()

      expect(Jira).not.toBeCalled()
      expect(userProperties.set).not.toBeCalled()
    })
  })

  describe('findInstancesByProject()', () => {
    it('Should return an empty array when userProperties doesn\'t have `jiraStore` value', () => {
      expect(jiraProjects.findInstancesByProject('DEV')).toHaveLength(0)
    })

    it('Should return multiple Jira instances which has matched project key', () => {
      const jsonString = JSON.stringify({
        sites: [
          {
            instanceHost: 'test.atlassian.net',
            projectsKeys: ['DEV', 'TEST'],
          },
          {
            instanceHost: 'hello.atlassian.net',
            projectsKeys: ['ABC', 'DEV', 'XYZ'],
          },
          {
            instanceHost: 'dummy.atlassian.net',
            projectsKeys: ['ABC'],
          },
        ],
      })

      userProperties.get
        .mockReturnValueOnce({ chunksReferences: ['dummy'] })
        .mockReturnValueOnce(jsonString)

      expect(jiraProjects.findInstancesByProject('DEV')).toEqual([
        'test.atlassian.net',
        'hello.atlassian.net',
      ])
    })
  })
})
