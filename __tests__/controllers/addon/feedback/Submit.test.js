import FeedbackSubmitController from 'src/controllers/addon/feedback/Submit'
import Gas from 'src/common/net/Gas'
import FeebackSuccessView from 'src/views/addon/feedback/Success'

jest.mock('src/common/net/Gas')
jest.mock('src/views/addon/feedback/Success')
jest.mock('src/common/analytics.js')

const params = {
  user: Symbol('user'),
  allowContactBack: 'allow',
  rating: '1',
  missingFeatures: 'Lorem ipsum dolor',
  ratingReason: '',
}

const payload = {
  name: 'feedback',
  server: '-',
  product: 'atlassian-gmail-integration',
  user: params.user,
  serverTime: Date.now(),
  properties: {
    allowContactBack: params.allowContactBack,
    rating: params.rating,
    missingFeatures: params.missingFeatures,
    ratingReason: params.ratingReason,
  },
}

const send = jest.fn().mockReturnThis()

Gas.mockImplementation(() => ({ send }))

describe('Controller addon.feedback.Submit', () => {
  it('Should post correct data via GAS', () => {
    new FeedbackSubmitController().execute(params)

    expect(Gas).toBeCalled()
    expect(send).toBeCalledWith(payload)
    expect(FeebackSuccessView).toBeCalled()
  })
})
