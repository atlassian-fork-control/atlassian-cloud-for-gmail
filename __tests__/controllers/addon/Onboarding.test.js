import ShowController from 'src/controllers/addon/Show'
import OnboardingController from 'src/controllers/addon/Onboarding'
import { userProperties } from 'src/common/properties'

jest.mock('src/controllers/addon/Show')
jest.mock('src/common/properties')

const params = { onboardingState: 'onboardingState' }
const event = {
  messageMetadata: {
    messageId: 'messageId',
    accessToken: 'accessToken',
  },
}

const executeReturnData = Symbol('entities')
const execute = jest.fn().mockReturnValue(executeReturnData)
ShowController.mockImplementation(() => ({ execute }))

describe('Controller addon.Onboarding', () => {
  beforeEach(() => jest.clearAllMocks())

  it('Should return correct data from addon.Show controller', () => {
    const entities = new OnboardingController({ event }).execute(params)

    expect(entities).toBe(executeReturnData)
  })

  it('Should call addon.Show controller with correct parameters', () => {
    new OnboardingController({ event }).execute(params)

    expect(ShowController).toBeCalledWith({ event })
  })

  it('Show check that the execute method of addon.Show controller has been called', () => {
    new OnboardingController({ event }).execute(params)

    expect(execute).toBeCalled()
  })

  it('Should update user properties', () => {
    new OnboardingController({ event }).execute(params)
    expect(userProperties.set).toBeCalledWith('onboardingState', params.onboardingState)
  })
})
