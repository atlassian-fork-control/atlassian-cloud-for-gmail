import UserShowController from 'src/controllers/jira/user/Show'
import Jira from 'src/common/net/Jira'
import UserView from 'src/views/jira/User'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Jira')
jest.mock('src/views/jira/User')

const user = Symbol('user')

const getUser = jest.fn().mockReturnValue(user)
const params = {
  instanceHost: 'dummy.atlassian.net',
  userName: 'username',
}

Jira.mockImplementation(() => ({ getUser }))

describe('Controller jira.user.Show', () => {
  beforeEach(() => {
    Jira.mockClear()
    UserView.mockClear()
  })

  it('Show return user view', () => {
    expect(new UserShowController().execute(params)).toBeInstanceOf(UserView)
  })

  it('Should call Jira with correct params', () => {
    new UserShowController().execute(params)

    expect(Jira).toBeCalledWith('dummy.atlassian.net')
    expect(getUser).toBeCalledWith({
      username: 'username',
      fields: 'avatarUrls,displayName,emailAddress,name,timeZone',
    })
  })

  it('Show call UserCard view with correct params', () => {
    new UserShowController().execute(params)

    expect(UserView).toBeCalledWith({
      instanceHost: params.instanceHost,
      user,
    })
  })
})
