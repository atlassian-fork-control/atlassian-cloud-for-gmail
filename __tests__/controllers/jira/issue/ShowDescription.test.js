import IssueDescriptionShowController from 'src/controllers/jira/issue/ShowDescription'
import Jira from 'src/common/net/Jira'
import IssueDescriptionView from 'src/views/jira/issue/Description'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Jira')
jest.mock('src/views/jira/issue/Description')

const params = {
  instanceHost: 'dummy-domain.atlassian.net',
  issueKey: 'DEV-1337',
}

const issueResponse = {
  key: params.issueKey,
  fields: { summary: 'bar' },
  renderedFields: { description: 'foo' },
}

const getIssue = jest.fn().mockReturnValue(issueResponse)
Jira.mockImplementation(() => ({ getIssue }))

describe('Controller jira.Issue.ShowDescription', () => {
  beforeEach(() => {
    Jira.mockClear()
    IssueDescriptionView.mockClear()
  })

  it('Should return issue description view', () => {
    const result = new IssueDescriptionShowController().execute(params)
    expect(result).toBeInstanceOf(IssueDescriptionView)
  })

  it('Should call Jira with correct params', () => {
    new IssueDescriptionShowController().execute(params)

    expect(Jira).toBeCalledWith(params.instanceHost)
    expect(getIssue).toBeCalledWith(params.issueKey, {
      fields: 'description,summary',
      expand: 'renderedFields',
    })
  })

  it('Should call Issue view with correct params', () => {
    new IssueDescriptionShowController().execute(params)
    expect(IssueDescriptionView).toBeCalledWith({
      issue: issueResponse,
      instanceHost: params.instanceHost,
    })
  })
})
