import CommentShowController from 'src/controllers/jira/issue/comment/Show'
import IssueCommentView from 'src/views/jira/issue/Comment'

jest.mock('src/views/jira/issue/Comment')

const params = {
  issue: 'issue',
  comment: 'comment',
  instanceHost: 'instance-domain.atlassian.net',
}

describe('Controller jira.comment.Show', () => {
  beforeEach(() => jest.clearAllMocks())

  it('Should return comment view', () => {
    expect(new CommentShowController().execute(params))
      .toBeInstanceOf(IssueCommentView)
  })

  it('Show call jira.issue.Comment view with correct parameters', () => {
    new CommentShowController().execute(params)

    expect(IssueCommentView).toBeCalledWith(params)
  })
})
