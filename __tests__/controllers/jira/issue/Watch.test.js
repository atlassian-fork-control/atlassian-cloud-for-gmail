import IssueShowController from 'src/controllers/jira/issue/Show'
import WatchController from 'src/controllers/jira/issue/Watch'
import Jira from 'src/common/net/Jira'
import IssueView from 'src/views/jira/issue/Issue'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Jira')
jest.mock('src/views/jira/issue/Issue')

const params = {
  instanceHost: 'dummy-domain.atlassian.net',
  issueKey: 'DEV-1337',
  watch: false,
  accountId: '12345',
}

const issue = {
  editmeta: { fields: {
    priorities: Symbol('priorities'),
    assignee: Symbol('assignee'),
    description: Symbol('description'),
  } },
}
const issueComments = {
  comments: Symbol('comments'),
  total: 1337,
}
const user = {
  account_id: Symbol('account_id'),
}

const getIssue = jest.fn().mockReturnThis()
const getIssueComments = jest.fn().mockReturnThis()
const getCurrentUser = jest.fn().mockReturnThis()
const getPriorities = jest.fn().mockReturnThis()
const addWatcher = jest.fn().mockReturnThis()
const removeWatcher = jest.fn().mockReturnThis()
const parallel = jest.fn().mockReturnThis()
const fetch = jest.fn().mockReturnValue([issue, issueComments, user])
Jira.mockImplementation(() => ({
  getIssue,
  getIssueComments,
  getCurrentUser,
  getPriorities,
  addWatcher,
  removeWatcher,
  parallel,
  fetch,
}))

describe('Controller jira.Issue.Watch', () => {
  beforeEach(() => {
    Jira.mockClear()
  })

  it('Should return issue view', () => {
    expect(new IssueShowController().execute(params)).toBeInstanceOf(IssueView)
  })

  it('Should remove watcher', () => {
    new WatchController().execute(params)

    expect(Jira).toBeCalledWith(params.instanceHost)
    expect(removeWatcher).toBeCalled()
  })

  it('Should add watcher', () => {
    new WatchController().execute({ ...params, watch: true })

    expect(Jira).toBeCalledWith(params.instanceHost)
    expect(addWatcher).toBeCalled()
  })
})
