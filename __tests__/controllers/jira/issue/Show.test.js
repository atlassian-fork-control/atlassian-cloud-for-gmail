import IssueShowController from 'src/controllers/jira/issue/Show'
import Jira from 'src/common/net/Jira'
import IssueView from 'src/views/jira/issue/Issue'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Jira')
jest.mock('src/views/jira/issue/Issue')

const params = {
  instanceHost: 'dummy-domain.atlassian.net',
  issueKey: 'DEV-1337',
}

const issue = {
  editmeta: { fields: {
    priorities: Symbol('priorities'),
    assignee: Symbol('assignee'),
    description: Symbol('description'),
  } },
}
const issueComments = {
  comments: Symbol('comments'),
  total: 1337,
  maxResults: 10,
}
const currentUser = {
  account_id: Symbol('account_id'),
}
const priorities = ['priority1', 'priority2']

const getIssue = jest.fn().mockReturnThis()
const getIssueComments = jest.fn().mockReturnThis()
const getCurrentUser = jest.fn().mockReturnThis()
const getPriorities = jest.fn().mockReturnThis()
const parallel = jest.fn().mockReturnThis()
const fetch = jest.fn().mockReturnValue([issue, issueComments, currentUser, priorities])
Jira.mockImplementation(() => ({
  getIssue, getIssueComments, getCurrentUser, getPriorities, parallel, fetch,
}))

describe('Controller jira.Issue.Show', () => {
  beforeEach(() => {
    Jira.mockClear()
    IssueView.mockClear()
  })

  it('Should return issue view', () => {
    expect(new IssueShowController().execute(params)).toBeInstanceOf(IssueView)
  })

  it('Should call Jira with correct params', () => {
    new IssueShowController().execute(params)

    expect(Jira).toBeCalledWith(params.instanceHost)
    expect(getIssue).toBeCalledWith(params.issueKey, {
      fields: 'project,issuetype,description,summary,status,assignee,labels,priority,timetracking,reporter,components,attachment,watches',
      expand: 'renderedFields,editmeta,transitions',
    })
    expect(getIssueComments).toBeCalledWith(params.issueKey, {
      maxResults: 10,
      orderBy: '-created',
      expand: 'renderedBody',
      fields: 'renderedBody,author,created',
    })
    expect(getCurrentUser).toBeCalled()
    expect(getPriorities).toBeCalled()
  })

  it('Should call Issue view with correct params', () => {
    new IssueShowController().execute(params)
    expect(IssueView).toBeCalledWith({
      issue,
      instanceHost: params.instanceHost,
      issueComments,
      currentUser,
      priorities,
      editableFields: Object.keys(issue.editmeta.fields),
    })
  })
})
