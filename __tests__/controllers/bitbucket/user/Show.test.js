import UserShowController from 'src/controllers/bitbucket/user/Show'
import Bitbucket from 'src/common/net/Bitbucket'
import UserView from 'src/views/bitbucket/User'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/views/bitbucket/User')

const user = Symbol('user')
const getUser = jest.fn().mockReturnValue(user)
Bitbucket.mockImplementation(() => ({ getUser }))

const params = {
  username: 'test-user',
}

describe('Controller bitbucket.user.Show', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Should return user view', () => {
    expect(new UserShowController().execute(params)).toBeInstanceOf(UserView)
  })

  it('Should call Bitbucket with correct params', () => {
    new UserShowController().execute(params)

    expect(getUser).toBeCalledWith(params.username)
  })

  it('Should call User view with correct params', () => {
    new UserShowController().execute(params)

    expect(UserView).toBeCalledWith({ user })
  })
})
