import IssueShowDescriptionController from 'src/controllers/bitbucket/issue/ShowDescription'
import IssueDescriptionView from 'src/views/bitbucket/issue/Description'

jest.mock('src/common/analytics')
jest.mock('src/views/bitbucket/issue/Description')

const issue = Symbol('issue')

describe('Controller bitbucket.issue.Show', () => {
  beforeEach(() => {
    IssueDescriptionView.mockClear()
  })

  it('Should return Bitbucket issue view', () => {
    const result = new IssueShowDescriptionController().execute({ issue })
    expect(result).toBeInstanceOf(IssueDescriptionView)
  })

  it('Should call Bitbucket issue view with correct params', () => {
    new IssueShowDescriptionController().execute({ issue })
    expect(IssueDescriptionView).toBeCalledWith({ issue })
  })
})
