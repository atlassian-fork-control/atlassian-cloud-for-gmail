import CommentShowController from 'src/controllers/bitbucket/issue/comment/Show'
import IssueCommentView from 'src/views/bitbucket/issue/Comment'

jest.mock('src/views/bitbucket/issue/Comment')

const params = {
  issue: 'issue',
  comment: 'comment',
}

describe('Controller bitbucket.comment.Show', () => {
  beforeEach(() => jest.clearAllMocks())

  it('Should return comment view', () => {
    expect(new CommentShowController().execute(params))
      .toBeInstanceOf(IssueCommentView)
  })

  it('Show call bitbucket.issue.Comment view with correct parameters', () => {
    new CommentShowController().execute(params)

    expect(IssueCommentView).toBeCalledWith(params)
  })
})
