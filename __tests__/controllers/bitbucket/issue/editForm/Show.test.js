import EditFormShowController from 'src/controllers/bitbucket/issue/editForm/Show'
import EditFormView from 'src/views/bitbucket/issue/EditForm'

jest.mock('src/common/analytics')
jest.mock('src/views/bitbucket/issue/EditForm')

describe('Controller bitbucket.Issue.editForm.Show', () => {
  beforeEach(() => jest.clearAllMocks())

  it('Should return EditForm view', () => {
    const issue = Symbol('issue')
    expect(new EditFormShowController().execute({ issue })).toBeInstanceOf(EditFormView)
    expect(EditFormView).toBeCalledWith({ issue })
  })
})
