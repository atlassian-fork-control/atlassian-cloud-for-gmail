import Bitbucket from 'src/common/net/Bitbucket'
import IssueWatchController from 'src/controllers/bitbucket/issue/Watch'
import IssueShowController from 'src/controllers/bitbucket/issue/Show'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/controllers/bitbucket/issue/Show')

const params = {
  owner: 'owner',
  repo: 'repo',
  id: 'id',
}

const watchIssue = jest.fn()
Bitbucket.mockImplementation(() => ({ watchIssue }))

const issueView = Symbol('issue view')
const issueShowExecute = jest.fn().mockReturnValue(issueView)
IssueShowController.mockImplementation(() => ({ execute: issueShowExecute }))

describe('Bitbucket issue watch controller', () => {
  beforeEach(() => {
    Bitbucket.mockClear()
    IssueShowController.mockClear()
  })

  it('Should return issue.Show controller results', () => {
    expect(new IssueWatchController().execute({ ...params, watch: true })).toBe(issueView)
    expect(IssueShowController).toBeCalled()
    expect(issueShowExecute).toBeCalledWith(params)
  })

  it('Should call Bitbucket with correct params', () => {
    new IssueWatchController().execute(params)

    expect(Bitbucket).toBeCalledWith(params.owner, params.repo)
    expect(watchIssue).toBeCalledWith(params.id, true)
  })
})
