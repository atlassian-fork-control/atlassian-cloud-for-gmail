import IssueShowController from 'src/controllers/bitbucket/issue/Show'
import Bitbucket from 'src/common/net/Bitbucket'
import IssueView from 'src/views/bitbucket/issue/Issue'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/views/bitbucket/issue/Issue')

const params = {
  owner: 'owner',
  repo: 'repo',
  id: 'id',
}

const issue = Symbol('issue')
const attachmentsSize = Symbol('attachmentsSize')
const isWatching = Symbol('isWatching')
const comments = Symbol('commnets')

const getIssue = jest.fn().mockReturnThis()
const getIssueComments = jest.fn().mockReturnThis()
const parallel = jest.fn().mockReturnThis()
const fetch = jest.fn().mockReturnValue([issue, comments])
let mockBitbucketMethods = { getIssue, getIssueComments, parallel, fetch }
Bitbucket.mockImplementation(() => mockBitbucketMethods)

describe('Controller bitbucket.issue.Show', () => {
  beforeEach(() => {
    Bitbucket.mockClear()
    IssueView.mockClear()
  })

  it('Should return bitbucket issue view', () => {
    const getIssueWatchStatus = jest.fn().mockReturnValue(isWatching)
    const getIssueAttachments = jest.fn().mockReturnValue({ size: attachmentsSize })
    mockBitbucketMethods = { ...mockBitbucketMethods, getIssueAttachments, getIssueWatchStatus }

    expect(new IssueShowController().execute(params)).toBeInstanceOf(IssueView)
  })

  it('Should call Bitbucket with correct params', () => {
    const getIssueWatchStatus = jest.fn().mockReturnValue(isWatching)
    const getIssueAttachments = jest.fn().mockReturnValue({ size: attachmentsSize })
    mockBitbucketMethods = { ...mockBitbucketMethods, getIssueAttachments, getIssueWatchStatus }

    new IssueShowController().execute(params)

    expect(Bitbucket).toBeCalledWith(params.owner, params.repo)
    expect(getIssue).toBeCalledWith(params.id, {
      fields: '+repository.owner',
    })
    expect(getIssueComments).toBeCalledWith(params.id, {
      pagelen: 10,
      sort: '-created_on',
      q: 'content.raw != null AND content.raw != ""',
      fields: 'values.user.display_name,values.created_on,values.content.html,size,pagelen',
    })
    expect(getIssueAttachments).toBeCalledWith(params.id, { fields: 'size' })
  })

  it('Should call Bitbucket issue view with correct params when user is watching the issue', () => {
    new IssueShowController().execute(params)
    expect(IssueView).toBeCalledWith({
      issue, comments, attachmentsSize, isWatching: true,
    })
  })

  it('Should call Bitbucket issue view with correct params when user is not watching the issue', () => {
    const error = Object.assign(new Error(), { code: 'NOT_FOUND' })

    const getIssueWatchStatus = jest.fn(() => { throw error })
    const getIssueAttachments = jest.fn().mockReturnValue({ size: attachmentsSize })
    mockBitbucketMethods = { ...mockBitbucketMethods, getIssueAttachments, getIssueWatchStatus }

    new IssueShowController().execute(params)
    expect(IssueView).toBeCalledWith({
      issue, comments, attachmentsSize, isWatching: false,
    })
  })

  it('Should return issue view when watch status and attachments are not available', () => {
    const error = Object.assign(new Error(), { code: 'FORBIDDEN' })

    const getIssueWatchStatus = jest.fn(() => { throw error })
    const getIssueAttachments = jest.fn(() => { throw error })
    mockBitbucketMethods = { ...mockBitbucketMethods, getIssueAttachments, getIssueWatchStatus }

    new IssueShowController().execute(params)
    expect(IssueView).toBeCalledWith({
      issue, comments, attachmentsSize: undefined, isWatching: undefined,
    })
  })

  it('Should throw an error while fetching issue status', () => {
    const error = Object.assign(new Error(), { code: 'UNAUTHORIZED' })

    const getIssueWatchStatus = jest.fn(() => { throw error })
    const getIssueAttachments = jest.fn().mockReturnValue({ size: attachmentsSize })
    mockBitbucketMethods = { ...mockBitbucketMethods, getIssueAttachments, getIssueWatchStatus }

    const execute = () => new IssueShowController().execute(params)

    expect(execute).toThrow(error)
    expect(execute).toThrowErrorMatchingSnapshot()
  })

  it('Should throw an error while fetching issue attachments', () => {
    const error = Object.assign(new Error(), { code: 'UNAUTHORIZED' })

    const getIssueWatchStatus = jest.fn().mockReturnValue(isWatching)
    const getIssueAttachments = jest.fn(() => { throw error })
    mockBitbucketMethods = { ...mockBitbucketMethods, getIssueAttachments, getIssueWatchStatus }

    const execute = () => new IssueShowController().execute(params)

    expect(execute).toThrow(error)
    expect(execute).toThrowErrorMatchingSnapshot()
  })
})
