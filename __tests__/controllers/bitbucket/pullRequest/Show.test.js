import PrShowController from 'src/controllers/bitbucket/pullRequest/Show'
import Bitbucket from 'src/common/net/Bitbucket'
import PrView from 'src/views/bitbucket/pullRequest/PullRequest'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/views/bitbucket/pullRequest/PullRequest')

const params = {
  owner: 'owner',
  repo: 'repo',
  id: 'id',
}

const pullRequest = Symbol('pull request')
const statuses = Symbol('statuses')
const currentUser = Symbol('current user')
const comments = Symbol('commnets')

const getPullRequest = jest.fn().mockReturnThis()
const getPullRequestStatusList = jest.fn().mockReturnThis()
const getPullRequestComments = jest.fn().mockReturnThis()
const getCurrentUser = jest.fn().mockReturnThis()
const parallel = jest.fn().mockReturnThis()
const fetch = jest.fn().mockReturnValue([pullRequest, statuses, comments, currentUser])
Bitbucket.mockImplementation(() => ({
  getPullRequest, getPullRequestStatusList, getPullRequestComments, getCurrentUser, parallel, fetch,
}))

describe('Controller bitbucket.pullRequest.Show', () => {
  beforeEach(() => {
    Bitbucket.mockClear()
    PrView.mockClear()
  })

  it('Should return pull request view', () => {
    expect(new PrShowController().execute(params)).toBeInstanceOf(PrView)
  })

  it('Should call Bitbucket with correct params', () => {
    new PrShowController().execute(params)

    expect(Bitbucket).toBeCalledWith(params.owner, params.repo)
    expect(getPullRequestStatusList).toBeCalledWith(params.id, {
      fields: 'values.description,values.key,values.state,values.refname,size',
    })
    expect(getPullRequest).toBeCalledWith(params.id, {
      fields: '+destination.repository.is_private,+destination.repository.owner',
    })
    expect(getCurrentUser).toBeCalled()
    expect(getPullRequestComments).toBeCalledWith(params.id, {
      pagelen: 10,
      sort: '-created_on',
      q: 'deleted != true AND content.raw != null AND content.raw != ""',
      fields: 'values.id,values.user.display_name,values.created_on,values.content.html,values.parent,values.inline,size,pagelen',
    })
  })

  it('Should call PullRequest view with correct params', () => {
    new PrShowController().execute(params)
    expect(PrView).toBeCalledWith({ pullRequest, statuses, comments, currentUser })
  })
})
