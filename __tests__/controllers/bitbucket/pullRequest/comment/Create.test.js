import Bitbucket from 'src/common/net/Bitbucket'
import CommentCreateController from 'src/controllers/bitbucket/pullRequest/comment/Create'
import PullRequestShowController from 'src/controllers/bitbucket/pullRequest/Show'
import AddonError from '../../../../../src/common/AddonError'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/controllers/bitbucket/pullRequest/Show')

const params = {
  owner: 'owner',
  repo: 'repo',
  id: 'id',
}

const extendedParams = {
  ...params,
  content: 'lorem ipsum',
}

const createPullRequestComment = jest.fn()
Bitbucket.mockImplementation(() => ({ createPullRequestComment }))

describe('Controller bitbucket.comment.Create', () => {
  beforeEach(() => {
    Bitbucket.mockClear()
  })

  it('Should call Bitbucket with correct params', () => {
    new CommentCreateController().execute(extendedParams)

    expect(Bitbucket).toBeCalledWith(params.owner, params.repo)
    expect(createPullRequestComment).toBeCalledWith(params.id, {
      content: extendedParams.content,
    })
  })

  it('Should call pullRequest.Show controller', () => {
    const execute = jest.fn()
    PullRequestShowController.mockImplementation(() => ({ execute }))

    new CommentCreateController().execute(extendedParams)

    expect(PullRequestShowController).toBeCalled()
    expect(execute).toBeCalledWith(params)
  })

  it('Should throw AddonError when no content provided', () => {
    const execute = () => new CommentCreateController().execute({ ...extendedParams, content: '' })

    expect(execute).toThrow(AddonError)
    expect(execute).toThrowErrorMatchingSnapshot()
  })
})
