import Bitbucket from 'src/common/net/Bitbucket'
import PreviewController from 'src/controllers/bitbucket/pullRequest/Preview'
import pullRequest from '../../../helpers/mocks/responses/bitbucket/getPullRequest.json'

jest.mock('src/common/net/Bitbucket')

const params = {
  owner: 'owner',
  repo: 'repo',
  id: 'id',
}

const getPullRequest = jest.fn().mockReturnValue(pullRequest)
Bitbucket.mockImplementation(() => ({ getPullRequest }))

describe('Controller bitbucket.pullRequest.Preview', () => {
  beforeEach(() => Bitbucket.mockClear())

  it('Should call bitbucket', () => {
    new PreviewController().execute(params)

    expect(Bitbucket).toBeCalledWith(params.owner, params.repo)
    expect(getPullRequest).toBeCalledWith(params.id, {
      fields: 'destination.repository.name,title,id,state,updated_on',
    })
  })

  it('Should return preview object', () => {
    expect(new PreviewController().execute(params)).toEqual({
      key: 'Pull Request #1338',
      source: 'test-repo',
      status: 'OPEN',
      title: 'Test pull request',
      updatedAt: '2018-02-06T11:53:38.380309+00:00',
    })
  })
})
