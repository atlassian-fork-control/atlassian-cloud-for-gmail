import PullRequestMergeShowController from 'src/controllers/bitbucket/pullRequest/merge/Show'
import PullRequestMergeView from 'src/views/bitbucket/pullRequest/Merge'
import Bitbucket from 'src/common/net/Bitbucket'

jest.mock('src/views/bitbucket/pullRequest/Merge')
jest.mock('src/common/net/Bitbucket')

const pullRequest = Symbol('pull request')
const getPullRequest = jest.fn().mockReturnValue(pullRequest)
Bitbucket.mockImplementation(() => ({ getPullRequest }))

describe('Controller bitbucket.pullRequest.merge.Show', () => {
  beforeEach(() => Bitbucket.mockClear())

  it('Should return pull request merge view', () => {
    expect(new PullRequestMergeShowController().execute(pullRequest))
      .toBeInstanceOf(PullRequestMergeView)
  })

  it('Should call bitbucket.pullRequest.merge.Show view with correct parameters', () => {
    new PullRequestMergeShowController().execute({ pullRequest })

    expect(PullRequestMergeView).toBeCalledWith({ pullRequest })
  })
})
