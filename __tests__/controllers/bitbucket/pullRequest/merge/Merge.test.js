import PullRequestMergeController from 'src/controllers/bitbucket/pullRequest/merge/Merge'
import PullRequestShowController from 'src/controllers/bitbucket/pullRequest/Show'
import Bitbucket from 'src/common/net/Bitbucket'
import AddonError from 'src/common/AddonError'

jest.mock('src/controllers/bitbucket/pullRequest/Show')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/common/analytics')

const params = {
  owner: 'owner',
  repo: 'repo',
  id: 'id',
}

const mergeParams = {
  merge_strategy: 'merge_commit',
  close_source_branch: true,
}

const pullRequestView = Symbol('pull request view')
const pullRequestShowExecute = jest.fn().mockReturnValue(pullRequestView)
PullRequestShowController.mockImplementation(() => ({ execute: pullRequestShowExecute }))

describe('Controller pullRequest.merge.Merge', () => {
  beforeEach(() => {
    Bitbucket.mockClear()
    PullRequestShowController.mockClear()
  })

  it('Should merge pull request', () => {
    const mergePullRequest = jest.fn()
    Bitbucket.mockImplementation(() => ({ mergePullRequest }))

    new PullRequestMergeController().execute({
      ...params,
      mergeStrategy: mergeParams.merge_strategy,
      closeSourceBranch: mergeParams.close_source_branch,
    })

    expect(Bitbucket).toBeCalledWith(params.owner, params.repo)
    expect(mergePullRequest).toBeCalledWith(params.id, mergeParams)
  })

  it('Should return bitbucket.pullRequest.Show results', () => {
    expect(new PullRequestMergeController().execute({ ...params, ...mergeParams }))
      .toBe(pullRequestView)
    expect(PullRequestShowController).toBeCalled()
    expect(pullRequestShowExecute).toBeCalledWith(params)
  })

  it('Should throw AddonError when request is bad and response message exists', () => {
    const error = Object.assign(new Error(), {
      response: { body: { error: { message: 'message' } } },
    })

    const mergePullRequest = jest.fn(() => { throw error })
    Bitbucket.mockImplementation(() => ({ mergePullRequest }))

    const execute = () => new PullRequestMergeController().execute({ ...params, ...mergeParams })

    expect(execute).toThrow(AddonError)
    expect(execute).toThrowErrorMatchingSnapshot()
  })

  it('Should throw AddonError when request is bad abut no response message', () => {
    const error = new Error()

    const mergePullRequest = jest.fn(() => { throw error })
    Bitbucket.mockImplementation(() => ({ mergePullRequest }))

    const execute = () => new PullRequestMergeController().execute({ ...params, ...mergeParams })

    expect(execute).toThrow(error)
    expect(execute).toThrowErrorMatchingSnapshot()
  })
})
