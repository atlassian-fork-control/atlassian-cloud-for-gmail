import Bitbucket from 'src/common/net/Bitbucket'
import PipelineRerunController from 'src/controllers/bitbucket/pipeline/Rerun'
import PipelineShowController from 'src/controllers/bitbucket/pipeline/Show'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/controllers/bitbucket/pipeline/Show')

const params = {
  owner: 'owner',
  repo: 'repo',
  commitHash: 'commitHash',
  branchName: 'branchName',
  id: 'id',
}

const rerunPipeline = jest.fn().mockReturnValue({ uuid: '{dummy}' })
Bitbucket.mockImplementation(() => ({ rerunPipeline }))

const pipelineView = Symbol('pipeline view')
const pipelineShowExecute = jest.fn().mockReturnValue(pipelineView)
PipelineShowController.mockImplementation(() => ({ execute: pipelineShowExecute }))

describe('Controller bitbucket.pipeline.Rerun', () => {
  beforeEach(() => {
    Bitbucket.mockClear()
    PipelineShowController.mockClear()
  })

  it('Returns pipeline.Show controller results', () => {
    expect(new PipelineRerunController().execute(params)).toBe(pipelineView)
    expect(PipelineShowController).toBeCalled()
    expect(pipelineShowExecute).toBeCalledWith({
      id: '{dummy}',
      owner: params.owner,
      repo: params.repo,
    })
  })

  it('Calls Bitbucket with correct params', () => {
    new PipelineRerunController().execute(params)
    expect(Bitbucket).toBeCalledWith(params.owner, params.repo)
    expect(rerunPipeline).toBeCalledWith(params.commitHash, params.branchName)
  })
})
