import IssueEditFormView from 'src/views/bitbucket/issue/EditForm'
import issue from '../../../helpers/mocks/responses/bitbucket/getIssue.json'

describe('View Issue/EditForm', () => {
  it('Should check that the issue editForm card data is correct', () => {
    const card = new IssueEditFormView({ issue }).render()

    expect(card.printJson()).toMatchSnapshot()
  })
})
