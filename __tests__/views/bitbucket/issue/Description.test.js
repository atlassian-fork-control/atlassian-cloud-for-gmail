import { Card } from 'gas-mock-service'
import { cloneDeep } from 'lodash'
import IssueDescriptionView from 'src/views/bitbucket/issue/Description'
import issue from '../../../helpers/mocks/responses/bitbucket/getIssue.json'

describe('View Issue Description', () => {
  it('Runs the render method and checks the data of used services', () => {
    const card = new IssueDescriptionView({ issue }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })

  it('Runs the render method and checks the data of used services when no description provided', () => {
    const clonedIssue = cloneDeep(issue)
    clonedIssue.content.html = ''
    const card = new IssueDescriptionView({ issue: clonedIssue }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
