import { Card } from 'gas-mock-service'
import CommentView from 'src/views/bitbucket/issue/Comment'
import issue from '../../../helpers/mocks/responses/bitbucket/getIssue.json'
import comments from '../../../helpers/mocks/responses/bitbucket/getIssueComments.json'

describe('View Issue Comment', () => {
  it('Runs the render method and checks the data of used services', () => {
    const card = new CommentView({
      issue,
      comment: comments.values[0],
    }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
