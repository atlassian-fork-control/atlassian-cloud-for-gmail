import { Card } from 'gas-mock-service'
import PullRequestDescriptionView from 'src/views/bitbucket/pullRequest/Description'
import pullRequest from '../../../helpers/mocks/responses/bitbucket/getPullRequest.json'

describe('View Pull Request Description', () => {
  it('Runs the render method and checks the data of used services', () => {
    const card = new PullRequestDescriptionView({ pullRequest }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
