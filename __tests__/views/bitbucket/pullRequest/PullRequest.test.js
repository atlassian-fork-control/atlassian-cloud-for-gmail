import { Card } from 'gas-mock-service'
import PullRequestView from 'src/views/bitbucket/pullRequest/PullRequest'
import pullRequest from '../../../helpers/mocks/responses/bitbucket/getPullRequest.json'
import statuses from '../../../helpers/mocks/responses/bitbucket/getPullRequestStatusList.json'
import currentUser from '../../../helpers/mocks/responses/bitbucket/getCurrentUser.json'
import comments from '../../../helpers/mocks/responses/bitbucket/getPullRequestCommentsList.json'

describe('View PullRequest', () => {
  it('Runs the render method and checks the data of used services', () => {
    Object.assign({
      pagelen: 3,
      size: 5,
      values: comments.values.slice(0, 9),
    }, comments)

    const card = new PullRequestView({ pullRequest, statuses, comments, currentUser }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
