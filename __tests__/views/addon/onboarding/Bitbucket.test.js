import { Card } from 'gas-mock-service'
import BitbucketOnboardingView from 'src/views/addon/onboarding/Bitbucket'

describe('Bitbucket onboarding view', () => {
  it('Renders Bitbucket onboarding card', () => {
    const card = new BitbucketOnboardingView({ authUrl: 'authUrl' }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
