import PreviewsView from 'src/views/addon/Previews'

describe('View Previews', () => {
  it('Renders empty view', () => {
    const card = new PreviewsView({
      previews: [],
      forbiddenEntities: [],
    }).render()
    expect(card.printJson()).toMatchSnapshot()
  })

  it('Renders previews entities', () => {
    const previews = [
      {
        key: 'Pull Request #1338',
        source: 'test-repo',
        status: 'OPEN',
        title: 'Test pull request',
        updatedAt: '2018-02-06T11:53:38.380309+00:00',
        entity: {
          product: 'bitbucket',
          name: 'pullRequest',
          args: { foo: 'bar' },
        },
      },
      {
        key: 'DEV-14',
        source: 'Development',
        status: 'To Do',
        title: 'Test Issue summary',
        updatedAt: '2018-06-22T17:31:26.796+0300',
        entity: {
          product: 'jira',
          name: 'issue',
          args: { foo: 'bar' },
        },
        iconUrl: 'iconUrl',
      },
    ]

    const card = new PreviewsView({
      previews,
      forbiddenEntities: [],
    }).render()

    expect(card).toMatchSnapshot()
  })

  it('Renders authorization prompts', () => {
    const forbiddenEntities = [
      {
        product: 'jira',
        name: 'issue',
        args: { instanceHost: 'dummy.atlassian.net', issueKey: 'TEST-123' },
      },
      {
        product: 'bitbucket',
        name: 'pullRequest',
        args: { owner: 'test-owner', repo: 'test-repo', id: '123' },
      },
    ]

    const card = new PreviewsView({
      previews: [],
      forbiddenEntities,
      jiraAuthUrl: 'https://jira-auth.com',
      bitbucketAuthUrl: 'https://bitbucket-auth.com',
    }).render()

    expect(card).toMatchSnapshot()
  })
})
