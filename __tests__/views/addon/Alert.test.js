import AlertView from 'src/views/addon/Alert'

describe('View Blank', () => {
  it('Renders card', () => {
    const card = new AlertView({ text: 'test', title: 'Error', type: 'error' }).render()

    expect(card.printJson()).toMatchSnapshot()
  })
})
