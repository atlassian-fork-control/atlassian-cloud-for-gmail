import { Card } from 'gas-mock-service'
import UserView from 'src/views/jira/User'
import user from '../../helpers/mocks/responses/jira/getUser.json'

describe('View jira.userCard.Show', () => {
  it('Runs the render method and checks the returned data is a card instance', () => {
    const card = new UserView({ instanceHost: 'main-jira-in-the-w0rld.atlassian.net', user }).render()

    expect(card).toBeInstanceOf(Card)
  })

  it('Runs the render method and checks the data of card service is correct', () => {
    const card = new UserView({ instanceHost: 'main-jira-in-the-w0rld.atlassian.net', user }).render()

    expect(card.printJson()).toMatchSnapshot()
  })
})
