global.OAuth2 = {
  createService: jest.fn().mockReturnThis(),
  setAuthorizationBaseUrl: jest.fn().mockReturnThis(),
  setTokenUrl: jest.fn().mockReturnThis(),
  setClientId: jest.fn().mockReturnThis(),
  setClientSecret: jest.fn().mockReturnThis(),
  setCallbackFunction: jest.fn().mockReturnThis(),
  setPropertyStore: jest.fn().mockReturnThis(),
  setCache: jest.fn().mockReturnThis(),
  hasAccess: jest.fn().mockReturnValue(true),
  getAccessToken: jest.fn().mockReturnValue('DUMMY'),
}
