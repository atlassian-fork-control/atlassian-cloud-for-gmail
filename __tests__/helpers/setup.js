import 'gas-mock-service'

// globals
import './globals/OAuth2'
import './globals/Utilities'

Date.now = jest.fn().mockReturnValue(1530000000000) // random static date
